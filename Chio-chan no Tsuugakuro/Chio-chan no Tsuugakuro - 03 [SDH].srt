﻿1
00:00:01,584 --> 00:00:07,590
♪〜

2
00:01:22,290 --> 00:01:28,296
〜♪

3
00:01:33,968 --> 00:01:36,054
（ちお）
ちょっと早く出た朝は

4
00:01:36,179 --> 00:01:38,556
公園を歩いてみる
ってのもいいわね

5
00:01:38,681 --> 00:01:40,016
朝から いろんな人が…

6
00:01:40,725 --> 00:01:41,559
（ちお）ん？

7
00:01:41,851 --> 00:01:43,520
（ちお）あっ
（男性たちの騒ぎ声）

8
00:01:43,770 --> 00:01:45,104
バイク…

9
00:01:45,438 --> 00:01:48,399
ってことは
暴走族の一味って感じかしら？

10
00:01:48,525 --> 00:01:52,695
なんか大声で騒いでる
朝からやだわ〜

11
00:01:52,821 --> 00:01:54,322
まあ 公園広いし

12
00:01:54,447 --> 00:01:56,199
（ちお）適当に よけて…
（安藤(あんどう)）出会っちまったんだよ

13
00:01:56,324 --> 00:01:57,826
（安藤）本物ってやつにな

14
00:01:58,243 --> 00:02:00,495
（ちお）
えっと 今日の１限目 何だっけ？

15
00:02:00,620 --> 00:02:02,664
（安藤）
そいつの名は
ブラッディ･バタフライ

16
00:02:03,081 --> 00:02:04,040
（ちお）ん？

17
00:02:04,165 --> 00:02:05,542
ブラ… うわっ

18
00:02:05,667 --> 00:02:08,837
（暴走族１）
ブラッディ･バタフライ？
一体 何者なんですか？

19
00:02:08,962 --> 00:02:10,672
（安藤）俺を一撃で倒した—

20
00:02:11,589 --> 00:02:13,049
女の名前だ

21
00:02:13,550 --> 00:02:15,260
（暴走族たち）お… 女？

22
00:02:16,177 --> 00:02:17,470
（ちお）あの人…

23
00:02:18,221 --> 00:02:19,806
あのときの！

24
00:02:22,308 --> 00:02:24,352
（安藤）
引き止めてくれるのは うれしいが

25
00:02:24,477 --> 00:02:28,231
そのブラッディ･バタフライに
族はやめると誓ったんだ

26
00:02:28,356 --> 00:02:30,108
（暴走族２）そんな！
（安藤）二言はない

27
00:02:30,441 --> 00:02:31,401
（ちお）
え〜！

28
00:02:31,526 --> 00:02:36,030
あれって この間の私の話を
真に受けてるってこと？

29
00:02:36,614 --> 00:02:39,450
（安藤）
初めは イカれた女が
絡んできたと思ったが

30
00:02:39,701 --> 00:02:41,035
近づいた瞬間

31
00:02:41,161 --> 00:02:43,288
ブラッディ･バタフライは
ニヤリと笑い

32
00:02:43,663 --> 00:02:46,833
気付いたら 俺は地面に
はいつくばってたぜ

33
00:02:47,083 --> 00:02:48,042
（暴走族３）ウソだろ

34
00:02:48,167 --> 00:02:50,461
（暴走族４）
この“鬼神”といわれた
安藤さんが？

35
00:02:51,004 --> 00:02:53,882
（暴走族１）
信じられねえ
ブラッディ･バタフライ！

36
00:02:54,007 --> 00:02:55,049
（暴走族２）しっかし

37
00:02:55,174 --> 00:02:57,343
ブラッディ･バタフライなんて
初耳だな

38
00:02:57,468 --> 00:03:00,597
（暴走族３）
ブラッディ･バタフライ…
よそのシマの人間か？

39
00:03:00,722 --> 00:03:02,223
（暴走族４）
ブラッディ･バタフライ

40
00:03:02,348 --> 00:03:04,350
バタフライナイフと何か関係が？

41
00:03:04,517 --> 00:03:08,479
（ちお）
やめて〜！
そんなに連呼しないで〜！

42
00:03:08,605 --> 00:03:09,480
ブラッディ･バタフライって

43
00:03:09,606 --> 00:03:11,566
ただのネットゲームの
プレーヤー名なのに

44
00:03:11,691 --> 00:03:14,277
リアルで
そんな真剣に言われると

45
00:03:14,569 --> 00:03:16,696
恥ずかしすぎる！

46
00:03:16,946 --> 00:03:19,616
お前ら 変に探ろうとするな

47
00:03:20,241 --> 00:03:21,576
（安藤）
ブラッディ･バタフライは
闇社会に生きる人間なんだ

48
00:03:21,576 --> 00:03:24,245
（安藤）
ブラッディ･バタフライは
闇社会に生きる人間なんだ

49
00:03:21,576 --> 00:03:24,245
（ちお）うぉ〜！

50
00:03:24,913 --> 00:03:27,248
（安藤）
俺はな 気付いちまったのさ

51
00:03:27,373 --> 00:03:29,292
ブラッディ･バタフライと
話していて

52
00:03:29,417 --> 00:03:32,170
俺たちがやってることが
ガキの遊びだってことにな

53
00:03:32,420 --> 00:03:33,588
（ちお）
ほああ〜！

54
00:03:33,880 --> 00:03:35,673
正直 シビれたぜ

55
00:03:35,798 --> 00:03:37,175
世の中に こんな—

56
00:03:37,300 --> 00:03:40,178
ブラッディ･バタフライみたいな
女がいるなんてな

57
00:03:40,428 --> 00:03:41,596
（ちお）
ああ〜！

58
00:03:41,846 --> 00:03:43,640
あんななりで この国 背負って

59
00:03:43,765 --> 00:03:46,059
よその国のギャング マフィアと
張り合ってるんだ

60
00:03:46,684 --> 00:03:48,519
ブラッディ･バタフライは！

61
00:03:49,312 --> 00:03:53,149
安藤さんが ここまで言う
人間なんて いたのか

62
00:03:53,274 --> 00:03:55,860
（暴走族１）
いや どうしても納得いかないです

63
00:03:56,194 --> 00:03:58,863
ブラッディ･バタフライを
この目で確かめさせてください！

64
00:03:58,988 --> 00:04:01,574
バカ野郎！
俺が信用できねえのか？

65
00:04:01,824 --> 00:04:04,869
（ちお）
うわあ！
だんだん話が大きくなって…

66
00:04:05,161 --> 00:04:07,497
このままじゃ 町を歩けなくなる

67
00:04:07,872 --> 00:04:11,167
俺は 安藤さんの強さを信じてます

68
00:04:11,542 --> 00:04:12,377
だからこそ

69
00:04:12,502 --> 00:04:14,754
ブラッディ･バタフライを
引きずり出してでも…

70
00:04:14,879 --> 00:04:15,713
乗るぜ

71
00:04:16,172 --> 00:04:19,092
絶対 何かひきょうな手を
使ったに違いない

72
00:04:19,217 --> 00:04:21,636
でなきゃ
安藤さんが負けるなんて！

73
00:04:21,886 --> 00:04:25,473
（暴走族１）
俺たちで… 俺たちの手で
必ず見つけ出す！

74
00:04:25,598 --> 00:04:28,351
（暴走族たち）
ブラッディ･バタフライ狩りだ！

75
00:04:29,185 --> 00:04:30,395
（安藤）あああ…

76
00:04:30,520 --> 00:04:31,562
（暴走族１）安藤さん？

77
00:04:32,689 --> 00:04:33,898
（暴走族１）あの…
（安藤）動くな！

78
00:04:34,607 --> 00:04:37,777
何も話すな
決して 振り向くな

79
00:04:39,070 --> 00:04:39,988
んっ

80
00:04:43,116 --> 00:04:46,119
ど… どこから
現れやがった

81
00:04:47,078 --> 00:04:49,163
ブラッディ･バタフライ！

82
00:04:50,206 --> 00:04:51,958
（暴走族たち）
こっ これが…

83
00:04:53,459 --> 00:04:56,212
ブラッディ･バタフライ！

84
00:04:57,463 --> 00:05:01,551
え？　これが
ブラッディ･バタフライ？

85
00:05:01,676 --> 00:05:05,346
なんか 想像と違う

86
00:05:05,555 --> 00:05:08,266
（暴走族１）
もっと
女コマンドー的な感じだと…

87
00:05:08,516 --> 00:05:11,644
（ちお）
フウ… さんざん
持ち上げてくれたおかげで

88
00:05:11,769 --> 00:05:14,063
うまく拍子抜けしてくれた

89
00:05:14,397 --> 00:05:16,774
これで話がしやすくなったわね

90
00:05:16,899 --> 00:05:20,778
何の用だ？
ブラッディ･バタフライ

91
00:05:21,279 --> 00:05:25,158
お前に 話があって来た

92
00:05:26,451 --> 00:05:28,745
２人きりで話がしたい

93
00:05:28,870 --> 00:05:29,954
（安藤）あっ ああ！

94
00:05:34,584 --> 00:05:35,835
お前らは動くな！

95
00:05:37,086 --> 00:05:38,212
すぐ戻る

96
00:05:39,297 --> 00:05:40,214
安藤さん！

97
00:05:40,339 --> 00:05:41,758
（安藤）そこで待ってろ

98
00:05:43,342 --> 00:05:46,095
（安藤）で？　話ってのは何だ？

99
00:05:46,554 --> 00:05:48,431
俺は 約束どおりチームを抜ける

100
00:05:48,556 --> 00:05:49,932
あ… あの

101
00:05:50,058 --> 00:05:51,809
（安藤）ん？　もしかして

102
00:05:51,934 --> 00:05:53,895
俺を裏の世界へ引き込もうと…

103
00:05:54,020 --> 00:05:56,064
あっ そう それ！

104
00:05:56,647 --> 00:05:58,941
なっ 買いかぶりすぎだ

105
00:05:59,067 --> 00:06:02,028
俺は 銃なんぞ握ったことも
人を殺す覚悟も…

106
00:06:02,153 --> 00:06:03,321
（ちお）それ！　それ それ！

107
00:06:03,696 --> 00:06:05,239
（安藤）
まして 国を背負うなんて…

108
00:06:05,364 --> 00:06:07,200
そう！　その辺の話

109
00:06:07,658 --> 00:06:09,786
全部 勘違いなんですよ

110
00:06:13,748 --> 00:06:15,833
（安藤）ん？
（ちお）ああっ そうなりますよね

111
00:06:16,292 --> 00:06:19,879
あの つまり そういう闇の組織とか
全部なくて

112
00:06:20,004 --> 00:06:22,423
普通の女子高生なんですよ 私

113
00:06:22,548 --> 00:06:23,758
中の下ポジションの

114
00:06:25,176 --> 00:06:26,010
ハハッ

115
00:06:26,135 --> 00:06:27,678
（ちお）
いや 冗談とかじゃなくて！

116
00:06:27,887 --> 00:06:31,766
そもそも あの マフラー？
ってとこに 足が触れて…

117
00:06:31,891 --> 00:06:32,850
あの おじさんが…

118
00:06:32,975 --> 00:06:36,229
廃人っていうのは ゲームに
どっぷりハマってる人のこと…

119
00:06:40,858 --> 00:06:42,485
（ちお）
落ち込んでる

120
00:06:42,985 --> 00:06:46,239
というか
割と話が通じる人でよかった

121
00:06:46,364 --> 00:06:48,032
そもそも この間 乗り切れたのも

122
00:06:48,157 --> 00:06:51,410
この人が話を聞いてくれる
タイプだったからだし…

123
00:06:51,536 --> 00:06:52,411
（安藤）なあ

124
00:06:52,745 --> 00:06:54,455
（安藤）確認するぞ
（ちお）はい！

125
00:06:54,580 --> 00:06:57,458
（安藤）
つまり お前は ただのパンピーで

126
00:06:57,583 --> 00:07:01,337
素通りするつもりだった不良に
運悪く絡まれちまい

127
00:07:02,880 --> 00:07:06,467
運とハッタリだけで
その不良を完全にだまし

128
00:07:07,009 --> 00:07:10,346
ビビらせ 暴走族をやめさせた

129
00:07:10,847 --> 00:07:13,141
そして だまされた不良は

130
00:07:13,307 --> 00:07:16,936
アホみてえに 仲間に
そのハッタリを振りまいて

131
00:07:17,395 --> 00:07:18,312
はう！

132
00:07:18,438 --> 00:07:22,733
ピエロに成り下がってると…
そういうことだな？

133
00:07:23,151 --> 00:07:25,278
（ちお）
ヤバい　きっと頭突きが来る

134
00:07:25,695 --> 00:07:26,821
よけられない！

135
00:07:27,738 --> 00:07:30,032
逆に スゲえ〜！

136
00:07:30,324 --> 00:07:31,534
あ…

137
00:07:33,578 --> 00:07:37,665
クッ フフフ…
まったく 大したもんだぜ

138
00:07:37,790 --> 00:07:38,708
ハハハハ

139
00:07:39,292 --> 00:07:41,627
いけねえ
あいつらに聞こえちまう

140
00:07:41,878 --> 00:07:42,837
なるほどな

141
00:07:42,962 --> 00:07:46,507
見た目どおり
普通の女子高生だったってわけだ

142
00:07:46,632 --> 00:07:48,843
クッ ハハハハ！

143
00:07:48,968 --> 00:07:51,554
どうりで
ブラッディ･バタフライって名前

144
00:07:51,679 --> 00:07:53,556
ダセえと思ったんだよな

145
00:07:53,931 --> 00:07:56,267
ブラッディ･バタフライ… ハハ！

146
00:07:56,392 --> 00:07:58,686
それを さっきまで 俺も真顔で…

147
00:07:58,811 --> 00:08:01,772
ブラッディ･バタフライ…
ハハハ！

148
00:08:01,898 --> 00:08:06,068
だから その話が広がらないように
したいんですけど！

149
00:08:06,194 --> 00:08:07,487
（安藤）分かってる 分かってる

150
00:08:07,778 --> 00:08:12,074
いや〜 チームに対する
多少の心残りも 吹き飛んだぜ

151
00:08:12,450 --> 00:08:14,911
完全に お前の勝ちだ

152
00:08:15,786 --> 00:08:19,332
だから 俺も
最後までピエロを演じてやるよ

153
00:08:19,874 --> 00:08:21,375
ほら 戻るぞ

154
00:08:21,501 --> 00:08:22,335
（ちお）えっ？

155
00:08:22,460 --> 00:08:23,711
え？　えっ えっ…

156
00:08:23,836 --> 00:08:26,297
（安藤）大丈夫だ　お前は ただ…

157
00:08:27,465 --> 00:08:29,842
安藤さん 遅いな

158
00:08:30,343 --> 00:08:32,595
（暴走族２）
大丈夫だ
あの人には 心配なんて…

159
00:08:32,720 --> 00:08:34,388
（安藤）うわああ〜！
（暴走族たち）おっ

160
00:08:34,764 --> 00:08:35,806
（安藤）ああっ

161
00:08:35,932 --> 00:08:37,099
（暴走族たち）安藤さん！

162
00:08:37,517 --> 00:08:38,392
あいつは…

163
00:08:38,518 --> 00:08:40,978
ブラッディ･バタフライは
ヤバすぎる

164
00:08:41,103 --> 00:08:42,605
つ… 強すぎる

165
00:08:43,314 --> 00:08:44,815
一体 何が…

166
00:08:45,608 --> 00:08:46,442
あっ 来た！

167
00:08:47,527 --> 00:08:49,654
このままじゃ 皆殺しにされるぞ！

168
00:08:49,779 --> 00:08:51,822
ヤツはバケモンだ！

169
00:08:51,948 --> 00:08:54,492
（ちお）
ゆっくり歩く　ゆっくり歩く

170
00:08:54,951 --> 00:08:56,244
む… 無傷で

171
00:08:56,369 --> 00:08:58,037
安藤さんを 虫の息に…

172
00:08:58,162 --> 00:09:00,164
マ… マジじゃねえか！

173
00:09:00,289 --> 00:09:01,415
（安藤）あっ チョウだ！

174
00:09:01,707 --> 00:09:02,917
来るぞ ヤツの…

175
00:09:03,042 --> 00:09:06,963
ブラッディ･バタフライの
由来となった なんか… 必殺技！

176
00:09:07,088 --> 00:09:10,341
半径１キロ以内の人間は
なんか 死ぬ！

177
00:09:11,300 --> 00:09:14,053
お前たち！　逃げろ〜！

178
00:09:14,178 --> 00:09:16,472
（暴走族たち）わああ〜！

179
00:09:16,639 --> 00:09:18,057
（安藤）いいか 忘れろ

180
00:09:18,182 --> 00:09:21,894
その名を二度と口にするな
絶対に関わるな！

181
00:09:22,478 --> 00:09:23,896
お前も逃げるんだ

182
00:09:24,021 --> 00:09:25,314
俺 逃げません！

183
00:09:25,439 --> 00:09:28,150
俺がブラッディ･バタフライを
倒します！

184
00:09:28,317 --> 00:09:30,987
安藤さんのためなら
この命 捨てても…

185
00:09:31,821 --> 00:09:34,657
逃げろってんだろ　殺すぞ！

186
00:09:35,074 --> 00:09:36,492
ひい…

187
00:09:36,659 --> 00:09:40,371
（暴走族１）わあ〜！
（逃げる足音）

188
00:09:43,040 --> 00:09:44,292
（安藤）フウ

189
00:09:44,834 --> 00:09:46,794
まっ こんなもんかな

190
00:09:46,919 --> 00:09:49,213
あんだけ脅せば もう大丈夫だろ

191
00:09:49,505 --> 00:09:54,468
ったく バイク残していくヤツは
暴走族の風上にも置けん

192
00:09:54,594 --> 00:09:57,096
あの… ありがとうございます！

193
00:09:57,805 --> 00:09:58,639
ん？

194
00:09:59,223 --> 00:10:01,809
これ 使ってください

195
00:10:06,897 --> 00:10:10,693
（安藤）
そんなの預かっちまったら
また 話がややこしくならあ

196
00:10:11,068 --> 00:10:13,487
じゃあな ブラッディ･バタフライ

197
00:10:14,155 --> 00:10:16,574
だから それ言わないで！

198
00:10:18,117 --> 00:10:20,953
（真奈菜(まなな)）
そういえばさ この間 暴走族と

199
00:10:21,078 --> 00:10:24,040
バイクに乗ってきたヤツがいる
って話 あるじゃん？

200
00:10:24,165 --> 00:10:25,791
（ちお）え？　ああ うん

201
00:10:25,916 --> 00:10:26,959
それが？

202
00:10:27,084 --> 00:10:29,920
（真奈菜）
いや その暴走族 引き連れてた女が

203
00:10:30,046 --> 00:10:33,758
ブラッディなんとかって
名乗ったらしいんだけど

204
00:10:33,883 --> 00:10:36,719
なんか聞き覚えあんのよね〜

205
00:10:37,053 --> 00:10:39,472
昔 ゲームにハマってたころ？

206
00:10:39,805 --> 00:10:43,392
（ちお）
うん その名前で一緒に
プレイしてたからね

207
00:10:43,517 --> 00:10:45,186
ちおちゃん 知ってる？

208
00:10:45,561 --> 00:10:47,396
さあね

209
00:10:48,898 --> 00:10:51,817
（ちお）
次は こいつを始末しなければ

210
00:10:57,865 --> 00:10:59,116
（ちお）
ダサいって言われた

211
00:11:00,034 --> 00:11:02,328
じゃあ あの人のチーム名
何てのよ？

212
00:11:02,912 --> 00:11:04,038
これだ！

213
00:11:05,373 --> 00:11:06,874
まさかの
非ドキュンネーム

214
00:11:06,999 --> 00:11:09,377
やだ
ちょっとかわいい

215
00:11:10,503 --> 00:11:12,254
（ちお）ふっ！　ふっ！

216
00:11:12,755 --> 00:11:13,589
真奈菜

217
00:11:13,714 --> 00:11:15,424
ちょっと腕を こっちに
出してくんない？

218
00:11:15,549 --> 00:11:17,468
（真奈菜）う… やだ

219
00:11:17,593 --> 00:11:18,928
（ちお）え〜？

220
00:11:19,053 --> 00:11:22,264
せっかく ひと晩かけて
ＣＱＣを練習したのに

221
00:11:22,515 --> 00:11:24,934
何それ？　またゲームの影響？

222
00:11:25,059 --> 00:11:27,269
つか 手 出してたら
どうなってたんだよ

223
00:11:27,686 --> 00:11:30,022
いや〜 アクションが
かっこいいのよ

224
00:11:30,147 --> 00:11:31,816
ムービーを何度も見返して

225
00:11:31,941 --> 00:11:35,986
う〜わ！　あんた ホントに
どどめ色の青春を過ごしてるわね

226
00:11:36,362 --> 00:11:38,948
（ちお）
こう来たら こう！
こう来たら こう！

227
00:11:39,073 --> 00:11:40,950
こう来たら こう！
つかまれたら こう！

228
00:11:41,075 --> 00:11:43,953
（真奈菜）
何これ 盆踊り？
高１の女子が何やってん…

229
00:11:44,078 --> 00:11:45,287
きええ〜！

230
00:11:45,538 --> 00:11:46,497
（真奈菜･ちお）うっ！

231
00:11:46,831 --> 00:11:48,833
（ちお）こうやって… こう！

232
00:11:50,918 --> 00:11:51,794
ほらほら 早く

233
00:11:51,919 --> 00:11:54,505
パソコンの画像フォルダの
パスワードを教えるんだ

234
00:11:54,630 --> 00:11:56,298
そっ それだけは…

235
00:11:56,424 --> 00:11:57,842
くっ… 殺せ！

236
00:11:58,092 --> 00:12:02,638
（真奈菜）
いや〜 しかし 実は ちおちゃんて
ムダに運動神経いいよな

237
00:12:03,639 --> 00:12:05,933
そういえば 明日 体力測定あるけど

238
00:12:06,058 --> 00:12:07,560
ちおちゃん 今年は どうするの？

239
00:12:08,060 --> 00:12:09,979
今年も流すかな

240
00:12:10,104 --> 00:12:11,981
ちおちゃん 一応 運動部なんだし

241
00:12:12,106 --> 00:12:15,109
せっかく動けるんだから
本気 出せばいいのに

242
00:12:15,234 --> 00:12:16,110
ダメだよ！

243
00:12:16,235 --> 00:12:17,653
１回 目立つと そこから先

244
00:12:17,778 --> 00:12:19,280
体育祭とか 球技大会とか

245
00:12:19,363 --> 00:12:20,906
ずっと期待されちゃうじゃない

246
00:12:21,198 --> 00:12:24,452
私は ひっそり
暮らしたいだけなのに〜

247
00:12:24,702 --> 00:12:27,037
出た　ちおちゃんの 中の下論

248
00:12:27,163 --> 00:12:30,749
一度の失敗も許されないの
この国は

249
00:12:31,000 --> 00:12:33,210
第一 体力測定あるあるといえば…

250
00:12:34,462 --> 00:12:37,548
反復横跳びって
動きとして面白すぎでしょ

251
00:12:37,673 --> 00:12:38,507
（真奈菜）いや…

252
00:12:38,632 --> 00:12:42,511
“体力測定あるある”って響きが
そもそも 新鮮すぎるけど

253
00:12:43,137 --> 00:12:46,432
全力で右往左往するって
変な踊りみたいっていうか…

254
00:12:46,557 --> 00:12:48,601
クッ ちおちゃん 全力で…

255
00:12:48,726 --> 00:12:50,436
こう！　ほら 分かる？

256
00:12:50,561 --> 00:12:52,688
ちおちゃん ちおちゃん
カバディって言って

257
00:12:52,813 --> 00:12:54,148
カバディ カバディ
カバディ カバディ！

258
00:12:54,273 --> 00:12:57,109
アハハハハ！　アハッ

259
00:12:57,234 --> 00:13:00,362
カバディ似合いすぎ
顔 作ってるし

260
00:13:00,488 --> 00:13:01,822
（ちお）疲れるんだって！

261
00:13:01,989 --> 00:13:05,409
カバディ ルール知らね〜
ちおちゃん 教えて

262
00:13:05,534 --> 00:13:07,203
（ちお）私も知らないよ

263
00:13:07,328 --> 00:13:09,663
（雪(ゆき)）おはよう　朝から元気だね

264
00:13:10,289 --> 00:13:11,624
ちょうどよかったよ

265
00:13:11,749 --> 00:13:12,583
（真奈菜･ちお）えっ 何が？

266
00:13:13,209 --> 00:13:14,502
この先輩がね

267
00:13:14,627 --> 00:13:17,671
（久志取(くしとり)）そう 鮫島(さめじま)学園３年

268
00:13:17,796 --> 00:13:21,425
カバディ部部長 久志取まどかだ

269
00:13:21,550 --> 00:13:24,887
（２人）
えっ カバディ… 部？　あっ！

270
00:13:25,179 --> 00:13:26,305
（２人）
やっちゃった

271
00:13:26,430 --> 00:13:27,681
（久志取）君たち さっきさ

272
00:13:27,806 --> 00:13:30,518
カバディのルール教えろって
言ってたよね？

273
00:13:30,643 --> 00:13:32,269
私も聞いたよ

274
00:13:32,436 --> 00:13:35,064
（２人）
いや あの… すみませんでした

275
00:13:35,189 --> 00:13:36,565
（久志取）うん いいよ

276
00:13:36,857 --> 00:13:38,609
悪気が
あったわけじゃないんだよね？

277
00:13:38,734 --> 00:13:39,568
（２人）はい！

278
00:13:39,693 --> 00:13:42,279
（久志取）
カバディに
興味があっただけなのよね？

279
00:13:42,571 --> 00:13:43,656
あ… はい

280
00:13:43,781 --> 00:13:46,492
（久志取）
カバディのルールを
知りたいんだよね？

281
00:13:47,493 --> 00:13:48,494
（２人）はい…

282
00:13:48,619 --> 00:13:51,413
よし！
じゃあ 私が教えてあげよう

283
00:13:52,081 --> 00:13:53,624
ちょっと裏 行こうか

284
00:13:53,749 --> 00:13:55,543
（雪）よかったね〜

285
00:13:55,668 --> 00:13:58,087
（ちお）
なんか めんどくさいことになった

286
00:13:58,504 --> 00:13:59,713
カバディ カバディ
カバディ カバディ

287
00:13:59,838 --> 00:14:01,215
カバディ カバディ
カバディ カバディ！

288
00:14:01,549 --> 00:14:02,716
こうやって 攻め側は

289
00:14:02,841 --> 00:14:05,261
敵陣にいる間
“カバディ”と言い続ける

290
00:14:05,386 --> 00:14:09,098
（ちお）
おお… マジで
カバディ カバディ言って〜ら

291
00:14:09,223 --> 00:14:12,017
（久志取）
そして 息が切れる前に
相手をタッチして

292
00:14:12,476 --> 00:14:15,813
自陣に戻ってくると
タッチした人数分が得点

293
00:14:15,938 --> 00:14:18,607
タッチされたプレーヤーは
場外行きとなる

294
00:14:18,732 --> 00:14:20,317
（ちお）なるほど

295
00:14:20,442 --> 00:14:22,528
攻め側が
交互に１人 出ていって

296
00:14:22,653 --> 00:14:24,530
それを 相手チームが
よける様子が

297
00:14:24,655 --> 00:14:26,532
鬼ごっこに
例えられるわけですね

298
00:14:26,657 --> 00:14:27,658
（久志取）そうだ

299
00:14:27,783 --> 00:14:29,743
（ちお）
うっわ！
どうでもいい！

300
00:14:30,160 --> 00:14:33,372
マジで登校中に
カバディ講座 始まっちゃったよ

301
00:14:33,497 --> 00:14:37,793
もうすぐ始業時間だし
早く解放してほしいんですけど

302
00:14:37,960 --> 00:14:41,755
（久志取）
鬼ごっこと違う点といえば
守備側にも 得点方法があって

303
00:14:42,214 --> 00:14:45,384
それは こうやって
鬼役を捕らえてしまうことだ

304
00:14:45,509 --> 00:14:48,679
完全に動きを止められれば
守備側の得点となる

305
00:14:49,096 --> 00:14:51,765
攻撃側はタッチ
守備側はキャッチ

306
00:14:51,891 --> 00:14:52,975
ひと言で言えば こうだ

307
00:14:53,100 --> 00:14:54,560
シンプルで いいですね

308
00:14:54,685 --> 00:14:55,644
勉強になりました

309
00:14:55,769 --> 00:14:57,563
（久志取）
よし！　じゃあ 模擬戦やるか

310
00:14:57,688 --> 00:14:58,564
へっ？

311
00:14:58,689 --> 00:15:00,357
君たち 負けたら入部ね

312
00:15:00,482 --> 00:15:01,525
ええっ！

313
00:15:01,859 --> 00:15:02,985
いや〜 カバディ部も

314
00:15:03,110 --> 00:15:06,822
私たちが引退したら
人数 足りなくてさ

315
00:15:06,947 --> 00:15:10,075
まさか カバディに興味がある
１年生がいたなんて

316
00:15:10,200 --> 00:15:11,243
よかった よかった

317
00:15:11,410 --> 00:15:13,037
えっ いや あの…

318
00:15:13,162 --> 00:15:15,873
あれ〜？　まさか 君たち

319
00:15:15,998 --> 00:15:20,586
カバディをバカにして
遊んでたわけじゃないんだよねえ？

320
00:15:20,794 --> 00:15:22,296
（ちお）
なるほど

321
00:15:22,421 --> 00:15:25,507
いやに丁寧に説明すると思ったら
これが狙いか

322
00:15:26,216 --> 00:15:30,054
う〜ん こっちには負い目があるし
相手は３年生

323
00:15:30,179 --> 00:15:32,431
断るのは難しい

324
00:15:32,556 --> 00:15:34,433
やるしかないのか…

325
00:15:34,558 --> 00:15:36,518
じゃ 君たち 攻め側ね

326
00:15:36,644 --> 00:15:37,853
よ〜し いけ！　真奈菜！

327
00:15:37,978 --> 00:15:39,104
えっ 私？

328
00:15:39,229 --> 00:15:40,272
出席番号順！

329
00:15:40,648 --> 00:15:42,232
え〜？　コホン

330
00:15:42,399 --> 00:15:43,943
カバッ カ… カバディ カバディ

331
00:15:44,068 --> 00:15:45,110
カバディ カバ… カバディ

332
00:15:45,319 --> 00:15:46,987
（真奈菜）
触って戻るだけでしょ？

333
00:15:48,280 --> 00:15:50,449
（久志取）えいっ！
（真奈菜）ブ〜ッ！

334
00:15:51,575 --> 00:15:54,161
ほ〜ら 暴れない 暴れない

335
00:15:54,286 --> 00:15:57,081
（ちお）
え〜！　さっきの説明と全然 違う

336
00:15:57,206 --> 00:16:00,626
完全に拘束されるまでは
逃げてもいいんだからね

337
00:16:00,751 --> 00:16:02,252
もう動けない

338
00:16:02,378 --> 00:16:03,963
（ちお）
めちゃめちゃ本気ですやん

339
00:16:04,755 --> 00:16:05,881
（ピピ〜ッ！）

340
00:16:06,507 --> 00:16:08,759
（ちお）
あの〜 確認したいんですけど

341
00:16:08,884 --> 00:16:10,803
どうなったら
決着がつくんでしたっけ？

342
00:16:11,178 --> 00:16:15,975
（久志取）
本来は時間制で 場外の選手が
復活できるタイミングもあるんだが

343
00:16:16,600 --> 00:16:18,435
もう 始業まで時間ないし

344
00:16:18,560 --> 00:16:22,022
どちらかのチームが
１回全滅したら 決着でいいだろ

345
00:16:22,147 --> 00:16:23,107
（ちお）
ええっ それって

346
00:16:23,232 --> 00:16:25,859
次に私がアウトになったら
負けってこと？

347
00:16:25,985 --> 00:16:27,778
（雪）じゃあ 私が攻め役ね

348
00:16:28,445 --> 00:16:30,030
（ちお）
つまり 細川(ほそかわ)さんに

349
00:16:30,155 --> 00:16:31,949
タッチされて逃げられたら

350
00:16:32,199 --> 00:16:34,159
その瞬間 カバディ部入部？

351
00:16:34,952 --> 00:16:37,538
かといって 細川さんを
捕まえようと思って

352
00:16:37,705 --> 00:16:40,416
下手に体が触れれば
タッチ扱いになる

353
00:16:40,958 --> 00:16:44,336
あれ？
カバディ 思ったより奥が深い？

354
00:16:44,753 --> 00:16:46,755
（雪）カバディ カバディ…
（ちお）逆に考えてみよう

355
00:16:47,589 --> 00:16:49,633
テニス部を辞めて
カバディ部に移ったほうが

356
00:16:49,758 --> 00:16:52,636
学園の中で過ごしやすくなる
ってこともあるかも

357
00:16:54,096 --> 00:16:57,516
（女子１）はじめまして
（女子２）新しいクラス 新鮮だね

358
00:16:57,641 --> 00:16:58,475
（女子１）三谷裳(みやも)さんって

359
00:16:58,600 --> 00:16:59,643
部活 何やってるの？

360
00:16:59,768 --> 00:17:02,021
うん カバディ部だよ

361
00:17:02,312 --> 00:17:03,981
（女子１）カバ？
（女子２）ディー？

362
00:17:04,231 --> 00:17:05,065
（男子１）カバディ カバディ！

363
00:17:05,190 --> 00:17:06,025
（男子２）からの〜？

364
00:17:06,150 --> 00:17:06,984
（男子１）タックル〜！

365
00:17:07,109 --> 00:17:08,902
（男子２）アッハハハハ！

366
00:17:09,028 --> 00:17:09,862
（ちお）ちょっと！

367
00:17:10,154 --> 00:17:11,780
カバディを
バカにしないで！

368
00:17:11,947 --> 00:17:14,366
それに タックルするのは
アンティ側だから

369
00:17:14,491 --> 00:17:16,618
キャントしてるんだったら
あなたはレイダーでしょ？

370
00:17:16,744 --> 00:17:18,454
み… 三谷裳さん？

371
00:17:18,579 --> 00:17:19,621
（男子１）何 言ってんの？　お前

372
00:17:20,080 --> 00:17:22,416
（ちお）
ダメだ！　私には 荷が重すぎる！

373
00:17:22,583 --> 00:17:25,377
あの子 なかなか いい動きするわね

374
00:17:25,502 --> 00:17:27,296
（雪）ハア… 息がもたない

375
00:17:27,546 --> 00:17:31,717
（ちお）
フウ… なんとか乗り切った
次は 私の攻撃の番か

376
00:17:32,384 --> 00:17:34,762
体力的に 長期戦は望みが薄い

377
00:17:34,887 --> 00:17:37,639
このターンで ２人まとめて
アウトにするしかない

378
00:17:37,890 --> 00:17:39,433
（久志取）
ん？　何だ？

379
00:17:40,267 --> 00:17:44,354
（ちお）
謎の珍スポーツに興じていると
思うから 本気が出ないんだ

380
00:17:44,855 --> 00:17:46,356
命を懸ける

381
00:17:46,899 --> 00:17:48,734
ゲーム脳 発動！

382
00:17:49,943 --> 00:17:53,322
カバディの謎ルールを
ビデオゲーム風に置き換える

383
00:17:53,739 --> 00:17:57,493
そう　私は触れたものを
爆弾にする能力者

384
00:17:57,868 --> 00:17:59,995
ミッションは
あの２人を爆弾に変えて

385
00:18:00,120 --> 00:18:01,997
安全圏に帰還すること

386
00:18:02,122 --> 00:18:03,040
（雪）早く来ないと
（ちお）もし 捕まれば

387
00:18:03,165 --> 00:18:04,750
（雪）ルール違反だよ〜
（ちお）拷問されて 死ぬ！

388
00:18:05,584 --> 00:18:08,670
（ちお）
この能力が発動するためには
呪文が必要

389
00:18:09,338 --> 00:18:10,672
“カバディ”

390
00:18:10,798 --> 00:18:12,091
（ちお）
よし 納得

391
00:18:12,257 --> 00:18:13,467
（ちお）カバディ カバディ…

392
00:18:13,592 --> 00:18:16,261
（久志取）
雪ちゃん 気をつけて
あの子…

393
00:18:16,887 --> 00:18:18,430
空気が変わった

394
00:18:18,597 --> 00:18:19,932
カバディ カバディ カバディ！

395
00:18:22,935 --> 00:18:25,437
（久志取）
いや 気迫はあるけど 所詮は素人

396
00:18:25,562 --> 00:18:26,563
深追いしすぎている

397
00:18:27,064 --> 00:18:28,941
こんな敵陣深くで タッチできても

398
00:18:29,066 --> 00:18:31,485
私と雪ちゃんの動きからは
逃げきれない

399
00:18:32,319 --> 00:18:33,153
後ろを取った！

400
00:18:33,946 --> 00:18:34,780
隙あり！

401
00:18:35,322 --> 00:18:36,657
雪ちゃん タックル！

402
00:18:36,824 --> 00:18:37,741
はい！

403
00:18:38,242 --> 00:18:40,160
（ちお）
つかまれたら こう！

404
00:18:40,911 --> 00:18:42,996
重心が動いたところを崩す！

405
00:18:43,122 --> 00:18:45,415
うう… 絶対に 離さない！

406
00:18:47,042 --> 00:18:49,378
（ちお）
細川さんは 性格どおり
まっすぐ来る

407
00:18:50,170 --> 00:18:52,005
（ちお）カ〜バディ〜！

408
00:18:52,631 --> 00:18:55,342
（ちお）
この手を剥がすためには… こう！

409
00:18:55,634 --> 00:18:56,885
（雪）はあ〜！

410
00:18:59,555 --> 00:19:00,681
あっ ああ…

411
00:19:05,769 --> 00:19:08,605
へっ？　あの… 久志取先輩？

412
00:19:08,981 --> 00:19:09,982
あの…

413
00:19:10,482 --> 00:19:11,316
えっ？

414
00:19:11,441 --> 00:19:13,485
（雪）
先輩！
三谷裳さん行っちゃいますよ？

415
00:19:13,652 --> 00:19:14,486
あれ？

416
00:19:14,611 --> 00:19:16,405
久志取先輩 手を 手を 手を！

417
00:19:19,950 --> 00:19:21,994
（ちお）
ミッション コンプリート

418
00:19:24,621 --> 00:19:26,582
２人タッチして帰還

419
00:19:26,707 --> 00:19:29,126
私の勝ちですね

420
00:19:29,376 --> 00:19:31,670
（久志取）
な… なぜ こんな作戦を…

421
00:19:31,795 --> 00:19:33,422
先輩 あの あの…

422
00:19:34,798 --> 00:19:37,593
最初に 先輩が真奈菜に
タックルしたとき

423
00:19:37,718 --> 00:19:40,095
あのときの表情を見て 思いました

424
00:19:40,345 --> 00:19:43,015
“ああ この人 女好きですやん”

425
00:19:43,599 --> 00:19:46,476
加えて 細川さんが
攻めているときも あなたは

426
00:19:46,602 --> 00:19:48,854
ヒラヒラするスカートの
中身を見ながら

427
00:19:48,979 --> 00:19:50,397
ニヤニヤ ニヤニヤ

428
00:19:50,522 --> 00:19:51,773
（雪）ええっ！

429
00:19:52,399 --> 00:19:54,234
だから
私をつかんだ手も

430
00:19:54,359 --> 00:19:56,486
細川さんの会陰(えいん)に
誘導すれば

431
00:19:56,612 --> 00:19:59,448
細川さんの会陰を
触り続けることを優先し

432
00:19:59,573 --> 00:20:01,825
必ず 離してくれると
思いました

433
00:20:02,451 --> 00:20:03,410
断言します

434
00:20:03,535 --> 00:20:04,786
あなたは カバディよりも

435
00:20:04,912 --> 00:20:07,372
女の子と くんずほぐれつ
するほうが好きなんだ！

436
00:20:07,581 --> 00:20:10,792
（久志取）
何を言うか！
私は カバディを愛している

437
00:20:11,084 --> 00:20:13,295
カバディをおいて
心を動かすものなど

438
00:20:13,378 --> 00:20:14,713
私にはない！

439
00:20:14,963 --> 00:20:18,967
じゃあ その腕を解いて
もう一戦やりましょう

440
00:20:19,218 --> 00:20:21,595
（久志取）うっ
（雪）んんん…

441
00:20:24,890 --> 00:20:26,558
ひっ！　あ…

442
00:20:29,228 --> 00:20:30,229
んん…

443
00:20:32,272 --> 00:20:33,440
ダメだ！

444
00:20:33,565 --> 00:20:37,569
雪ちゃんの会陰を これから先
人生で何度 触れるかと思うと

445
00:20:37,694 --> 00:20:39,321
体が 動かん！

446
00:20:39,446 --> 00:20:40,906
（雪）そこ お尻 お尻 お尻！

447
00:20:41,365 --> 00:20:43,617
（久志取）私の… 負けだ
（雪）ああ〜

448
00:20:44,743 --> 00:20:49,289
離してくださ〜い

449
00:20:50,916 --> 00:20:55,295
（久志取）
今回は カバディストとしての
不覚悟を反省させられたよ

450
00:20:55,545 --> 00:20:58,423
まあ その…
いろいろ すまなかった

451
00:20:58,548 --> 00:20:59,633
いえいえ

452
00:20:59,758 --> 00:21:03,262
あの 久志取先輩
１つ 質問いいですか？

453
00:21:03,387 --> 00:21:04,221
ああ

454
00:21:04,596 --> 00:21:07,140
どうやったら
そんな 乳デカくなります？

455
00:21:07,933 --> 00:21:11,520
（ちお）
こいつ この空気の中
よく そういうこと聞けるな

456
00:21:11,645 --> 00:21:13,063
やっぱり 病気なのか？

457
00:21:13,313 --> 00:21:15,899
（久志取）
いや 言われて気付いたが

458
00:21:16,024 --> 00:21:18,610
思い返せば カバディを始める前は

459
00:21:18,735 --> 00:21:21,196
私は いわば
普通の体形だった気が…

460
00:21:21,321 --> 00:21:22,155
（ちおたち）えっ！

461
00:21:22,281 --> 00:21:23,573
一体 何が？

462
00:21:24,408 --> 00:21:27,703
（久志取）
カバディは
インド発祥の競技というのもあって

463
00:21:27,828 --> 00:21:31,415
その動きの中には
ヨガの要素があるといわれている

464
00:21:31,957 --> 00:21:37,337
もしかしたら そのヨガの要素が
私の体に働きかけたのかもしれない

465
00:21:37,546 --> 00:21:38,547
（ちお･真奈菜）うっ！

466
00:21:41,591 --> 00:21:45,387
（ちお･真奈菜）
カバディ カバディ カバディ
カバディ カバディ カバディ…

467
00:21:48,974 --> 00:21:53,729
（久志取）
もっと もっと
カバディ道に精進しなければ

468
00:21:54,313 --> 00:21:57,399
でも 雪ちゃんのお尻…

469
00:21:57,607 --> 00:22:00,485
（久志取）
フッ フフフフ

470
00:21:57,607 --> 00:22:00,485
（ちお･真奈菜）
カバディ カバディ…

471
00:22:01,320 --> 00:22:07,326
♪〜

472
00:23:21,358 --> 00:23:27,364
〜♪

473
00:23:31,409 --> 00:23:32,869
（雪）
次回「ちおちゃんの通学路」は

474
00:23:32,994 --> 00:23:35,664
「スモーク･オン･ザ･セーラー」
「桜の花を手にとって」

475
00:23:35,997 --> 00:23:38,667
「まななちゃんの通学路」の
３本になります

476
00:23:38,792 --> 00:23:39,751
お楽しみに

477
00:23:39,876 --> 00:23:40,836
カバディ

