﻿1
00:00:01,167 --> 00:00:07,173
♪〜

2
00:01:21,873 --> 00:01:27,879
〜♪

3
00:01:36,638 --> 00:01:37,722
（ちお）真奈菜(まなな)

4
00:01:37,847 --> 00:01:39,474
細川(ほそかわ)さんのメール
見た？

5
00:01:39,599 --> 00:01:40,517
（真奈菜）あ〜

6
00:01:40,642 --> 00:01:43,728
カバディ部の部長が
消息不明ってやつ？

7
00:01:43,853 --> 00:01:44,687
（ちお）そうそう

8
00:01:45,188 --> 00:01:47,315
（ちお）
何か事件に巻き込まれたとか？

9
00:01:47,440 --> 00:01:49,109
（真奈菜）いや どうかな？

10
00:01:49,234 --> 00:01:50,985
（ザザザッ）
えっ 何？

11
00:01:51,402 --> 00:01:54,364
（真奈菜）四足歩行の動物？
（ちお）ん〜？

12
00:01:55,615 --> 00:01:57,242
ファ〜

13
00:01:57,367 --> 00:01:59,244
サモエドでもいた？

14
00:01:59,661 --> 00:02:00,495
（２人）あっ

15
00:02:00,620 --> 00:02:02,914
（久志取(くしとり)）ハア ハア

16
00:02:03,581 --> 00:02:05,166
（真奈菜･ちお）
先輩いた

17
00:02:05,458 --> 00:02:06,334
ハア…

18
00:02:07,043 --> 00:02:10,713
ハア ハア ハア…

19
00:02:10,839 --> 00:02:12,257
くっ まだだ

20
00:02:12,841 --> 00:02:14,092
（ちお）ねえ 何かやってる

21
00:02:14,217 --> 00:02:15,510
（真奈菜）し〜っ
（ちお）ん？

22
00:02:15,844 --> 00:02:16,928
（久志取）もうワンセット！

23
00:02:17,137 --> 00:02:18,680
カバディ！　カバディ！

24
00:02:18,805 --> 00:02:20,223
カバディ カバディ カバディ…

25
00:02:20,473 --> 00:02:22,100
カバディ！

26
00:02:25,311 --> 00:02:28,982
ハア ハア…

27
00:02:29,440 --> 00:02:32,485
こんな… こんなタックルでは

28
00:02:34,404 --> 00:02:35,780
もうひとカバだ！

29
00:02:35,905 --> 00:02:37,448
カバディ カバディ カバディ…

30
00:02:37,574 --> 00:02:39,784
（ちお）
えっ 何やってんの？

31
00:02:39,909 --> 00:02:41,035
（久志取）
カバディ カバディ カバディ…

32
00:02:41,661 --> 00:02:43,955
カバディ カバディ カバディ…

33
00:02:44,080 --> 00:02:47,208
（ちお）
まさか… ずっとここに？

34
00:02:48,126 --> 00:02:50,920
（老師）調子は どうかな？

35
00:02:51,254 --> 00:02:52,797
（久志取）老師様
（真奈菜･ちお）老師？

36
00:02:53,506 --> 00:02:56,217
（老師）水 くんできたぞ

37
00:02:57,302 --> 00:02:58,678
（２人）ううっ

38
00:03:04,350 --> 00:03:05,435
（久志取）フウ

39
00:03:05,852 --> 00:03:09,564
（老師）
お前さんが来てから １週間か

40
00:03:09,731 --> 00:03:12,525
生活には慣れたようじゃの

41
00:03:12,650 --> 00:03:14,444
はい おかげさまで

42
00:03:15,278 --> 00:03:17,655
山ごもりと意気込んで来たものの

43
00:03:17,947 --> 00:03:20,658
まさか 食べ物ひとつ
ままならないとは…

44
00:03:21,326 --> 00:03:23,077
そして こんな森の奥で

45
00:03:23,453 --> 00:03:26,372
まさか 人と出会えるとは
思いませんでした

46
00:03:27,373 --> 00:03:31,377
（ちお）
いや ここ
ただの住宅街の公園なんだけど

47
00:03:31,669 --> 00:03:34,047
本当に学校はいいのか？

48
00:03:34,172 --> 00:03:37,217
はい 技が完成するまでは
戻りません

49
00:03:38,509 --> 00:03:41,721
倒すべき… 相手がいるのです

50
00:03:42,472 --> 00:03:43,848
あれって まさか…

51
00:03:43,973 --> 00:03:44,807
（ちお）え？

52
00:03:44,933 --> 00:03:48,228
ちおちゃんにカバディで
負けたのが原因なんじゃない？

53
00:03:48,353 --> 00:03:50,063
いやいや そんなわけないでしょ

54
00:03:51,022 --> 00:03:54,317
だって あの勝負は
円満に解決したわけだし

55
00:03:55,735 --> 00:03:58,488
（久志取）いつか 必ず…

56
00:04:02,617 --> 00:04:04,285
（ちお）
私… かな？

57
00:04:04,535 --> 00:04:07,372
とはいえ
見つけちゃったわけだし

58
00:04:07,497 --> 00:04:10,124
説得して
学校に戻ってもらわないと

59
00:04:10,458 --> 00:04:12,835
細川さんも心配してるだろうし

60
00:04:12,961 --> 00:04:15,630
（ちお）
まあ でも
先輩 練習やめる気なさそう…

61
00:04:15,755 --> 00:04:16,631
あっ 蹴られた

62
00:04:17,382 --> 00:04:20,927
私が行くと こじれそうだから
ここは…

63
00:04:21,719 --> 00:04:22,845
（ちお）真奈菜が…
（真奈菜）えっ？

64
00:04:23,221 --> 00:04:25,181
適当に励ましてあげればいいから！

65
00:04:25,348 --> 00:04:27,058
（真奈菜）ちおちゃん？
（ちお）イグニッション オン

66
00:04:27,392 --> 00:04:29,310
真奈菜ロケット発射！

67
00:04:29,435 --> 00:04:31,020
（真奈菜）うわ〜っ

68
00:04:31,145 --> 00:04:33,106
おおっ　とっ　ほっ！

69
00:04:33,773 --> 00:04:34,691
（久志取）野々村(ののむら)？

70
00:04:34,816 --> 00:04:36,484
先輩 やっと見つけました

71
00:04:37,110 --> 00:04:38,653
話は聞かせてもらって…

72
00:04:39,112 --> 00:04:40,321
悩んでるって

73
00:04:40,446 --> 00:04:42,699
あ〜 その その…

74
00:04:42,824 --> 00:04:44,242
（真奈菜）
適当に励ます

75
00:04:45,451 --> 00:04:48,037
マイナースポーツのカバディなんて

76
00:04:48,162 --> 00:04:51,624
きっと それほど
真剣にやるものではないですよ

77
00:04:52,834 --> 00:04:53,668
ねっ

78
00:04:54,419 --> 00:04:55,461
（ちお）あ…

79
00:04:56,296 --> 00:04:57,880
（ちお）
何 言ってんだ？　あいつ

80
00:04:58,298 --> 00:05:01,301
プッ ダッハハハハ

81
00:05:01,426 --> 00:05:04,220
あれで励ましてるつもりなんじゃな

82
00:05:04,762 --> 00:05:07,682
面白い後輩を持っとるのう

83
00:05:08,641 --> 00:05:10,518
お〜い 久志取？

84
00:05:11,394 --> 00:05:13,313
（久志取）
１週間ぶりに見る…

85
00:05:14,230 --> 00:05:15,398
生身の…

86
00:05:16,232 --> 00:05:17,567
生身の…

87
00:05:19,027 --> 00:05:19,944
（ジュル）

88
00:05:20,236 --> 00:05:21,779
（久志取）はっ！
（真奈菜）久志取先輩？

89
00:05:22,322 --> 00:05:25,033
（老師）お前さんも大変じゃのう

90
00:05:25,158 --> 00:05:26,951
（真奈菜）あっ いえ…

91
00:05:28,286 --> 00:05:30,288
（久志取）
今 意識が…

92
00:05:30,913 --> 00:05:34,042
この感情はマズい… マズいぞ！

93
00:05:34,250 --> 00:05:35,668
（老師）トカゲは好きか？
（真奈菜）きゃっ

94
00:05:39,505 --> 00:05:41,549
（真奈菜）あれ？　先輩？
（久志取）は… 離れろ！

95
00:05:42,175 --> 00:05:43,593
（久志取）
煩悩を捨てろ！

96
00:05:44,260 --> 00:05:48,097
私は 邪念を捨てるために
ここに来たのだ

97
00:05:48,264 --> 00:05:51,309
真にカバディに打ち込むために！

98
00:05:51,642 --> 00:05:54,062
見なければ 大丈夫！

99
00:05:54,187 --> 00:05:56,355
私は 私は…

100
00:05:56,773 --> 00:05:58,983
この手で つかむんだ！

101
00:06:00,109 --> 00:06:02,570
この手で つかむ！

102
00:06:03,112 --> 00:06:04,072
つかむ

103
00:06:04,864 --> 00:06:05,990
つかむ

104
00:06:09,744 --> 00:06:11,454
（ちお）
つかまれたら こう！

105
00:06:11,579 --> 00:06:14,123
（久志取）
つかむ… つかむ！

106
00:06:14,874 --> 00:06:16,501
絶対に 離さない！

107
00:06:16,626 --> 00:06:17,460
（久志取）
つかむ

108
00:06:17,668 --> 00:06:19,712
（ちお）
細川さんは 性格どおり
まっすぐ来る

109
00:06:19,837 --> 00:06:21,297
（久志取）
つかむ…

110
00:06:22,757 --> 00:06:23,966
つかむ

111
00:06:25,760 --> 00:06:27,637
ああ〜っ

112
00:06:27,762 --> 00:06:28,888
つかむ！

113
00:06:30,473 --> 00:06:32,892
あっ あっ… つかむ

114
00:06:35,061 --> 00:06:36,521
つかむ

115
00:06:36,854 --> 00:06:37,772
（雪(ゆき)）久志取先輩？

116
00:06:38,481 --> 00:06:40,191
（久志取）
つかむ！

117
00:06:40,733 --> 00:06:42,610
（雪）
先輩！
三谷裳(みやも)さん行っちゃいますよ？

118
00:06:43,653 --> 00:06:45,696
久志取先輩 手を 手を 手を！

119
00:06:45,822 --> 00:06:48,783
（久志取）
ううっ あのとき

120
00:06:50,076 --> 00:06:54,747
つかんだ雪ちゃんのお尻
よかったな〜

121
00:06:54,914 --> 00:06:55,748
先輩？

122
00:06:56,415 --> 00:06:58,292
（久志取）
あっ ああっ いかん！

123
00:06:59,794 --> 00:07:00,795
ダ… ダメ

124
00:07:01,337 --> 00:07:02,713
あっ アハハハ

125
00:07:03,047 --> 00:07:03,881
いか〜ん！

126
00:07:04,132 --> 00:07:05,258
ハッ アハハ

127
00:07:06,050 --> 00:07:06,884
ダメ〜

128
00:07:07,260 --> 00:07:09,053
ハッ ハハ アハハッ

129
00:07:09,428 --> 00:07:10,471
いか〜ん！

130
00:07:10,805 --> 00:07:12,098
あっ ああ〜っ

131
00:07:12,807 --> 00:07:13,975
ヘヘヘヘ…

132
00:07:14,517 --> 00:07:17,103
ああっ ああっ 違う…

133
00:07:17,520 --> 00:07:18,938
うわ うわ うわ

134
00:07:19,063 --> 00:07:20,648
（久志取）うああ〜っ！
（真奈菜）何？　何なん？

135
00:07:20,898 --> 00:07:25,778
（久志取）
私は 女体に触れたくて
カバディをしてるわけでは…

136
00:07:26,112 --> 00:07:28,197
あああ…

137
00:07:28,906 --> 00:07:33,453
ワッハッハッハッ　ヘ〜ヘヘッ

138
00:07:33,578 --> 00:07:36,247
なるほど
おおむね 話は見えたわい

139
00:07:37,123 --> 00:07:38,291
老師様？

140
00:07:38,791 --> 00:07:41,419
（老師）昔話をしてやろう

141
00:07:41,669 --> 00:07:47,175
わしは これでも20年前は
名の通った企業戦士だったんじゃ

142
00:07:47,925 --> 00:07:52,763
しかし 毎日 毎日
会社の歯車として摩耗し

143
00:07:52,889 --> 00:07:59,520
金はあれど 心は完全に疲れ果てて
もう限界じゃった

144
00:07:59,645 --> 00:08:00,521
そんな朝

145
00:08:01,189 --> 00:08:04,650
電車で はしゃぐ
女子高生を見かけた

146
00:08:05,234 --> 00:08:08,821
彼女たちは
とてもエネルギーに満ちていて

147
00:08:09,155 --> 00:08:13,242
なんだか わしまで
元気を分けてもらえた気がした

148
00:08:13,451 --> 00:08:14,619
（女子Ａ）じゃあね
（女子Ｂ）じゃあね

149
00:08:15,077 --> 00:08:16,370
（老師）
だから…

150
00:08:19,874 --> 00:08:23,294
わしは ただ もう少しだけ…

151
00:08:24,003 --> 00:08:27,215
少しだけ 分けてほしかったんじゃ

152
00:08:27,840 --> 00:08:31,135
その日を乗り切るだけの

153
00:08:31,552 --> 00:08:34,722
ほんの少しのエネルギーを！

154
00:08:34,847 --> 00:08:36,891
（鉄格子が閉まる音）

155
00:08:39,977 --> 00:08:44,106
そして 全てを失い
わしは ここへ来た

156
00:08:44,607 --> 00:08:46,025
（久志取）そんな過去が…

157
00:08:46,317 --> 00:08:50,530
（老師）
久志取よ わしは お主が羨ましい

158
00:08:51,489 --> 00:08:52,490
いいか？

159
00:08:52,949 --> 00:08:57,662
女子高生に触れるのは
女子高生の特権なのじゃぞ

160
00:08:58,621 --> 00:09:02,333
お主の本当の気持ちは何じゃ？
久志取

161
00:09:03,167 --> 00:09:05,711
私の 私の…

162
00:09:06,796 --> 00:09:07,630
私は…

163
00:09:10,675 --> 00:09:12,009
老師様…

164
00:09:17,265 --> 00:09:18,432
お尻が

165
00:09:19,850 --> 00:09:21,435
もみたいです

166
00:09:23,104 --> 00:09:25,815
（ホイッスル）
（ゲーム音声）３ポイント

167
00:09:25,940 --> 00:09:29,735
（ちお）
ん〜っと
話は よく聞こえなかったけど

168
00:09:30,778 --> 00:09:34,156
解決したっぽい？
やるなあ 真奈菜

169
00:09:35,074 --> 00:09:39,662
じゃあ 先に細川さんに
お知らせして 手柄を横取りしよう

170
00:09:40,288 --> 00:09:43,791
（ちお）
事件が事件だけに
ポイントも大きいはず

171
00:09:44,458 --> 00:09:45,376
あっ ああ…

172
00:09:45,918 --> 00:09:47,253
よく言った

173
00:09:47,378 --> 00:09:51,382
どうじゃ？
己の気持ちを見つめ直すと

174
00:09:51,549 --> 00:09:54,218
何か見えるものがあるじゃろ？

175
00:09:54,343 --> 00:09:56,929
あの〜 誰か説明を

176
00:09:57,221 --> 00:10:00,099
はっ！　何だ？　この感覚は
（心拍音）

177
00:10:00,308 --> 00:10:05,271
分かる… 細胞の奥から
血が沸き立つ この…

178
00:10:05,813 --> 00:10:06,814
感覚！

179
00:10:06,939 --> 00:10:09,400
うお〜っ！

180
00:10:09,692 --> 00:10:10,526
はあっ！

181
00:10:11,110 --> 00:10:12,361
（老師）おおっ
（真奈菜）ううっ

182
00:10:12,486 --> 00:10:13,321
へ？

183
00:10:13,613 --> 00:10:17,199
今 完全に 理解した

184
00:10:17,450 --> 00:10:18,868
おお〜っ

185
00:10:19,619 --> 00:10:22,997
カバ道(どう)とは 揉(も)むことと
みつけたり！

186
00:10:24,999 --> 00:10:27,168
フォ〜ッ フォッフォッフォッ

187
00:10:27,293 --> 00:10:30,338
自分なりのカバ道を
みつけたようじゃの

188
00:10:30,713 --> 00:10:31,547
（真奈菜）
カバ道？

189
00:10:32,256 --> 00:10:34,967
老師様 そして野々村よ

190
00:10:36,886 --> 00:10:39,305
あなたたちのおかげで
開眼しました

191
00:10:39,430 --> 00:10:40,264
私も？

192
00:10:41,599 --> 00:10:45,478
今なら 見える気がする
カバディの奥義が

193
00:10:45,895 --> 00:10:47,563
（久志取）野々村
（真奈菜）はい！

194
00:10:47,688 --> 00:10:49,940
（久志取）
受けてくれないか？　私の技を

195
00:10:50,191 --> 00:10:51,192
ええっ？

196
00:10:51,317 --> 00:10:53,861
（久志取）
ケガはさせない
試してみたいんだ

197
00:10:54,195 --> 00:10:56,906
いくぞ〜！

198
00:10:57,281 --> 00:10:59,408
あっちに三谷裳さんが隠れてます！

199
00:10:59,533 --> 00:11:00,660
（久志取）とおっ

200
00:11:00,785 --> 00:11:05,539
裏切ったな〜！

201
00:11:05,790 --> 00:11:07,958
（ちお）
あれ？　全然 痛くない

202
00:11:08,959 --> 00:11:12,630
むしろ
羽毛布団に包まれてるような…

203
00:11:12,963 --> 00:11:15,174
ほう ありゃすごい

204
00:11:16,133 --> 00:11:20,221
（久志取）
ああ… 完成した
究極のタックルが！

205
00:11:20,429 --> 00:11:23,057
三谷裳！　久しぶりだな〜

206
00:11:23,224 --> 00:11:25,935
（ちお）あの勝負のことは許して…
（久志取）もういい！

207
00:11:26,060 --> 00:11:29,063
今はただ お前の尻をももうぞ

208
00:11:29,313 --> 00:11:31,107
（ちお）先輩 下ろしてください

209
00:11:31,190 --> 00:11:31,982
（久志取）ダメだ

210
00:11:33,442 --> 00:11:36,821
（久志取）
ハア ひともみする度

211
00:11:37,071 --> 00:11:41,325
高原にて清涼な空気を
深呼吸するように しみるぞ

212
00:11:42,034 --> 00:11:45,830
ああ 私はカバディがしたい

213
00:11:46,247 --> 00:11:50,126
地球上 全ての女カバディストの
尻の形を知りたい

214
00:11:50,376 --> 00:11:55,172
（真奈菜）あっ ああ…
（老師）なんと美しい光景じゃ

215
00:11:55,840 --> 00:11:58,092
（ちお）
なんとか助かる方法は…

216
00:11:58,634 --> 00:12:00,136
そうだ！　もうそろそろ…

217
00:12:00,469 --> 00:12:02,179
（雪）久志取先輩！
（ちお）はっ

218
00:12:03,097 --> 00:12:04,640
本当にいた

219
00:12:05,099 --> 00:12:07,101
三谷裳さんにメールもらって…

220
00:12:07,226 --> 00:12:08,060
（久志取）雪ちゃん？

221
00:12:08,602 --> 00:12:10,563
って 何してるんですか？

222
00:12:10,896 --> 00:12:13,023
ああ 雪ちゃん これはな

223
00:12:13,274 --> 00:12:14,108
そういえば！

224
00:12:14,692 --> 00:12:18,070
細川さんの体って
陸上部の大会が近いから

225
00:12:18,195 --> 00:12:20,781
ぷりっぷりに仕上がってるよね〜

226
00:12:21,115 --> 00:12:23,492
ぷりっぷり？

227
00:12:26,036 --> 00:12:26,871
先輩？

228
00:12:28,664 --> 00:12:29,498
えっ？

229
00:12:30,416 --> 00:12:33,794
（久志取）ヘヘヘ… ぷり…

230
00:12:34,378 --> 00:12:36,130
（久志取）うお〜っ！
（雪）きゃ〜っ

231
00:12:38,007 --> 00:12:39,300
ゲホホッ

232
00:12:39,425 --> 00:12:42,595
細川さんの足なら大丈夫
あとは…

233
00:12:43,179 --> 00:12:45,181
（真奈菜）はっ
（ちお）真奈菜 おら〜！

234
00:12:45,556 --> 00:12:46,599
あっ

235
00:12:46,724 --> 00:12:48,476
てめえのケツも もんでやる〜

236
00:12:48,601 --> 00:12:50,478
（老師）ホッホッホ〜

237
00:12:50,603 --> 00:12:54,565
いつの時代も
元気で 熱くて まぶしくて

238
00:12:54,690 --> 00:13:00,237
わしもまた 失った気持ちを
取り戻せたようじゃ

239
00:13:00,529 --> 00:13:04,074
（ちおと真奈菜の騒ぎ声）

240
00:13:04,825 --> 00:13:09,205
（老師）
やっぱ女子高生 最高！

241
00:13:14,543 --> 00:13:17,838
当て馬にするなんて
三谷裳さん ひどい

242
00:13:18,047 --> 00:13:20,716
ホントだよね
ホントひどかったよね

243
00:13:20,841 --> 00:13:23,511
（ちお）
細川さん ごめん
あのときは ああするしか…

244
00:13:24,512 --> 00:13:26,805
え〜 じゃあ 許します

245
00:13:26,931 --> 00:13:29,683
久志取先輩との追いかけっこ
楽しかったしね

246
00:13:29,808 --> 00:13:32,102
（真奈菜）えっ？
（ちお）うは〜っ！

247
00:13:32,228 --> 00:13:34,855
（ちお）
普段 真奈菜とつるんでて
素直に許されるなんてないから

248
00:13:34,980 --> 00:13:38,442
こんなとき何て言ったらいいか
分かんないよ〜

249
00:13:42,738 --> 00:13:45,658
（ちお）
いや〜 新作のゲーム
当たりだったわ〜

250
00:13:46,575 --> 00:13:48,827
３年待ったかいが
あったってもんよ

251
00:13:50,371 --> 00:13:51,997
敵国のスパイとして侵入

252
00:13:52,581 --> 00:13:57,670
テロ組織を壊滅させ
無事 アメリカを救ってやったぜ

253
00:13:58,254 --> 00:14:02,758
いや〜 それにしても洋ゲーって
自国を救わせるの好きよな

254
00:14:03,092 --> 00:14:08,264
え〜っと 日が昇る方向だから
東って あっちかな？

255
00:14:09,223 --> 00:14:11,308
感謝しろよ アメリカ！

256
00:14:12,476 --> 00:14:14,019
（ちお）
いや〜 しかし

257
00:14:15,479 --> 00:14:19,441
ゲームをやったあとの通学路って
気になっちゃうんだよね

258
00:14:19,733 --> 00:14:20,818
角が！

259
00:14:21,151 --> 00:14:24,738
特にＴ字路なんて
両側に敵がいたら挟み撃ちだから

260
00:14:24,864 --> 00:14:27,032
な〜んか気になっちゃうっていうか

261
00:14:28,492 --> 00:14:29,493
（たっくんママ）ん？

262
00:14:34,373 --> 00:14:35,207
（ちお）リーン！
（たっくんママ）えっ？

263
00:14:36,041 --> 00:14:37,042
（ちお）
敵は… いない

264
00:14:37,626 --> 00:14:39,837
（ちお）そして 素早く振り向き

265
00:14:40,254 --> 00:14:42,756
油断するな　油断するな

266
00:14:44,049 --> 00:14:44,925
よし！

267
00:14:45,259 --> 00:14:47,177
（たっくん）お〜っ

268
00:14:48,429 --> 00:14:49,388
あ…

269
00:14:52,016 --> 00:14:53,183
オールクリア

270
00:14:53,517 --> 00:14:55,561
応答せよ こちら安全を確認した

271
00:14:58,439 --> 00:15:00,983
う〜ん え〜っと

272
00:15:01,317 --> 00:15:05,654
たっくん あのおねえさんみたいに
ちゃんと安全確認するのよ

273
00:15:06,030 --> 00:15:06,947
うん！

274
00:15:10,409 --> 00:15:11,327
ん〜 あっ

275
00:15:11,911 --> 00:15:13,579
（ちお）
あれって真奈菜だ

276
00:15:13,704 --> 00:15:14,538
（ちお）お〜…

277
00:15:14,914 --> 00:15:15,956
（ちお）
ちょっと待てよ

278
00:15:16,957 --> 00:15:19,209
この地形 まるで

279
00:15:19,585 --> 00:15:22,963
潜入ゲーム鉄板の一本道！

280
00:15:23,589 --> 00:15:27,009
敵も こっちに気付いてないし
これは…

281
00:15:28,177 --> 00:15:30,387
（ちお）ま〜なな！
（真奈菜）ん？

282
00:15:31,263 --> 00:15:33,974
あれ？　今 ちおちゃんの声…

283
00:15:36,518 --> 00:15:39,063
（ちお）
ヘヘッ ここだよ〜

284
00:15:39,939 --> 00:15:44,360
真奈菜 待ってろよ〜
このまま近づいて…

285
00:15:45,694 --> 00:15:49,031
ゲームみたいに
サプライズ決めてやる！

286
00:15:49,281 --> 00:15:52,451
驚く顔が楽しみだぜ〜！

287
00:15:52,826 --> 00:15:56,330
（真奈菜）
ん？　気のせいかな
う〜ん…

288
00:15:56,830 --> 00:15:58,499
（ちお）よっ ほっ

289
00:16:00,584 --> 00:16:04,630
（ちお）
あれ？　思ったより
めちゃくちゃ しんどい！

290
00:16:05,756 --> 00:16:08,550
うお〜っ
おなかくらい深さありそう

291
00:16:08,842 --> 00:16:10,678
う〜ん　まあ いっか

292
00:16:11,220 --> 00:16:14,390
（ちお）
ヤバい！
全然ゲームみたいに進めない

293
00:16:14,515 --> 00:16:15,849
お… 落ちる

294
00:16:16,308 --> 00:16:17,685
ここで落ちたら

295
00:16:17,810 --> 00:16:20,354
ケガはしないまでも 全身ずぶぬれ

296
00:16:20,854 --> 00:16:23,983
かばんもぬれたら
教科書ガビガビになっちゃう！

297
00:16:24,191 --> 00:16:25,192
それはダメ！

298
00:16:25,651 --> 00:16:29,113
あ… 握力がもつうちに
進むのは諦めて…

299
00:16:30,155 --> 00:16:34,118
ってこれ よく見たら この柵
なんか地味に登りづらそうだし

300
00:16:34,410 --> 00:16:36,954
ぐぎぎぎぎ…

301
00:16:39,289 --> 00:16:40,249
まっ ま…

302
00:16:42,126 --> 00:16:43,002
（真奈菜）えっ？

303
00:16:43,419 --> 00:16:45,921
こっ これって まさか…

304
00:16:46,463 --> 00:16:47,798
生き霊(りょう)？

305
00:16:48,674 --> 00:16:50,801
何？　私を呪おうってわけ？

306
00:16:50,926 --> 00:16:53,095
どうせ この世を恨んでるんでしょ

307
00:16:53,220 --> 00:16:55,014
あんたなんて 全然 怖くないわ！

308
00:16:55,305 --> 00:16:59,184
あんたが大っ嫌いな この世界で
汗流して頑張って生きてる人間が

309
00:16:59,309 --> 00:17:02,563
いちばん強いんだから〜！

310
00:17:05,899 --> 00:17:08,110
おっ 除霊した？

311
00:17:08,861 --> 00:17:10,738
（ちお）そういうのいいから

312
00:17:10,863 --> 00:17:12,990
（真奈菜）ん？　まさか…

313
00:17:14,366 --> 00:17:15,868
はっ ちおちゃん！

314
00:17:16,368 --> 00:17:18,787
（ちお）助けて！　腕が もう…

315
00:17:20,831 --> 00:17:21,707
う〜ん

316
00:17:22,166 --> 00:17:23,125
（ちお）
“う〜ん”？

317
00:17:23,292 --> 00:17:25,878
ちょ… ちょっと 真奈菜
ふざけないで

318
00:17:26,003 --> 00:17:27,046
（真奈菜）ちおちゃんさ

319
00:17:27,713 --> 00:17:30,883
落ちても いいんじゃない？
ケガはしなさそうだし

320
00:17:31,091 --> 00:17:32,134
えっ？

321
00:17:33,635 --> 00:17:37,056
（真奈菜）
ちおちゃん どうせ私に
イタズラしようと思ったんでしょ

322
00:17:38,432 --> 00:17:42,311
そんなムチャしてたら
いつか大ケガするわよ

323
00:17:42,436 --> 00:17:46,315
ここいらで ちょっと
痛い目 見てもいいかもね

324
00:17:47,775 --> 00:17:50,861
勘違いしないで
心配して言ってるの

325
00:17:51,320 --> 00:17:53,781
（真奈菜）親友としてね
（ちお）なぜカメラを出す？

326
00:17:54,323 --> 00:17:59,661
（ちお）
ウッソ〜 てっきり こんな感じに
友情が確認できると思ったのに

327
00:18:00,162 --> 00:18:03,040
そうきますか いいでしょう

328
00:18:03,499 --> 00:18:07,336
分かったよ 真奈菜
私も覚悟が足りなかったよ

329
00:18:07,795 --> 00:18:11,090
最後の力を振り絞る！

330
00:18:11,340 --> 00:18:13,467
なあ 相棒！

331
00:18:15,719 --> 00:18:16,553
えっ？

332
00:18:17,096 --> 00:18:20,474
（ちお）一緒に 地獄に行こうぜ

333
00:18:20,891 --> 00:18:22,184
ヘヘヘッ

334
00:18:22,893 --> 00:18:23,977
あっ

335
00:18:24,978 --> 00:18:26,021
うっ
（カーン）

336
00:18:26,146 --> 00:18:27,439
あたたたた！

337
00:18:27,564 --> 00:18:29,274
ちょっ これヤバッ
は… 離して！

338
00:18:29,399 --> 00:18:30,651
嫌だ〜！

339
00:18:31,151 --> 00:18:33,654
てか そんなに動ける体力
どこにあったの？

340
00:18:33,779 --> 00:18:36,073
真奈菜に
ひと泡吹かせられると思ったら

341
00:18:36,532 --> 00:18:37,991
体が勝手に動いた！

342
00:18:38,158 --> 00:18:40,661
（真奈菜）
こいつ 生き霊よりタチが悪い

343
00:18:40,869 --> 00:18:42,412
（真奈菜）どうするのよ これ！

344
00:18:42,538 --> 00:18:44,540
（ちお）
乗り切ろう！　２人で協力して

345
00:18:44,665 --> 00:18:46,083
（真奈菜）はあ？　なんでよ！

346
00:18:46,333 --> 00:18:48,836
そもそも ちおちゃんが
勝手にやったことでしょ

347
00:18:48,961 --> 00:18:50,504
私を巻き込まないで！

348
00:18:50,754 --> 00:18:53,298
どうせ これも
ゲームの影響なんでしょ？

349
00:18:53,924 --> 00:18:55,092
それって いつかホントに

350
00:18:55,217 --> 00:18:56,802
ゲームオーバーに
なるってことじゃない？

351
00:18:56,927 --> 00:18:58,220
いいかげんにしてよ！

352
00:18:58,345 --> 00:19:00,848
どこまでバカなの？
子供じゃないんだから

353
00:19:00,973 --> 00:19:02,850
もう高校生だよ　分かってる？

354
00:19:02,975 --> 00:19:05,978
ホント頭 詰まってないんだから
ちおちゃんったら もう！

355
00:19:06,186 --> 00:19:09,940
（ちおの泣き声）

356
00:19:10,065 --> 00:19:10,899
（真奈菜）
泣いた！

357
00:19:11,024 --> 00:19:15,070
（ちお）
ま〜な〜な〜

358
00:19:15,195 --> 00:19:18,448
ウウ… ごめん〜

359
00:19:20,033 --> 00:19:25,539
真奈菜を
びっくりさせたかっただけなの〜

360
00:19:25,789 --> 00:19:27,916
エエ〜ン エエ〜ン

361
00:19:28,041 --> 00:19:29,418
分かった

362
00:19:29,626 --> 00:19:31,003
ウウ〜

363
00:19:31,128 --> 00:19:32,963
（真奈菜）２人で切り抜けよっか

364
00:19:34,173 --> 00:19:36,717
んで どうすりゃいいのよ？

365
00:19:37,050 --> 00:19:38,552
（ちお）えっと…

366
00:19:38,719 --> 00:19:41,054
まずは かばんを受け取って

367
00:19:41,263 --> 00:19:43,640
もう ほら 早く渡して

368
00:19:44,099 --> 00:19:45,309
真奈菜…

369
00:19:45,475 --> 00:19:47,019
（ちお）うっ うっ

370
00:19:48,353 --> 00:19:50,898
よし かばんオーケー
次は？

371
00:19:51,190 --> 00:19:53,817
（ちお）私の腕をつかんで
（真奈菜）こう？

372
00:19:54,318 --> 00:19:57,946
ウウ…
この手を絶対に離さないで！

373
00:19:58,238 --> 00:19:59,865
分かったから もう泣くな！

374
00:20:00,365 --> 00:20:01,950
思いっきり 引っ張って

375
00:20:02,367 --> 00:20:03,785
（真奈菜）せ〜の

376
00:20:03,911 --> 00:20:06,371
（２人）う〜っ う〜っ

377
00:20:06,788 --> 00:20:09,541
（ちお）
ぬおあああ　うう…

378
00:20:10,250 --> 00:20:11,960
うぎぎぎぎ…

379
00:20:12,502 --> 00:20:14,463
う〜っ

380
00:20:15,172 --> 00:20:20,802
ハア ハア ハア ハア…

381
00:20:21,178 --> 00:20:24,473
もう大丈夫　助かったよ

382
00:20:24,890 --> 00:20:26,308
（真奈菜）フウ
（ちお）本当に

383
00:20:27,226 --> 00:20:28,727
本当に ありがとうね

384
00:20:29,061 --> 00:20:32,397
じゃあ サプライズも
ある意味 成功ってことで

385
00:20:32,689 --> 00:20:35,192
（ちお）これで一緒に登校…

386
00:20:35,317 --> 00:20:36,151
あっ

387
00:20:36,568 --> 00:20:38,695
ごめん 気が抜け…

388
00:20:41,031 --> 00:20:42,741
真… 奈菜

389
00:20:42,991 --> 00:20:44,451
言ったでしょ

390
00:20:45,035 --> 00:20:47,621
絶対に ちおちゃんを離さないって

391
00:20:48,163 --> 00:20:49,414
（ちお）真奈菜

392
00:20:49,539 --> 00:20:52,584
早く引き戻してくれない… かな？

393
00:20:53,877 --> 00:20:57,422
（真奈菜）
いや 私も ちおちゃんを支えるのが
精いっぱいで

394
00:20:57,547 --> 00:21:00,467
足 もう もたないかも…

395
00:21:00,842 --> 00:21:05,347
（ちお）
いやいや 動けるの
真奈菜だけなんだから 頑張ってよ

396
00:21:05,764 --> 00:21:06,848
（真奈菜）ああ〜ん？

397
00:21:07,015 --> 00:21:09,393
せっかく助けたのに！
なに？ その態度

398
00:21:09,518 --> 00:21:11,770
まだ 助けられてませ〜ん
途中です

399
00:21:11,895 --> 00:21:14,564
はあ〜？　こっちは
あんたを離してもいいんだからね？

400
00:21:14,690 --> 00:21:17,192
もう２人とも落ちます〜
残念でした！

401
00:21:17,317 --> 00:21:19,236
嫌よ あんただけ落ちなさいよ！

402
00:21:19,403 --> 00:21:20,279
（ちお）無理 無理

403
00:21:20,404 --> 00:21:23,323
（真奈菜）
何なん？　ムッカつくわ
もう離す！　ぜって〜離す！

404
00:21:23,699 --> 00:21:24,992
（サラリーマン）おお…

405
00:21:25,117 --> 00:21:26,118
（ちお･真奈菜）はっ！

406
00:21:28,704 --> 00:21:31,081
助けてください！

407
00:21:36,211 --> 00:21:37,963
（真奈菜･ちお）ハア ハア

408
00:21:38,088 --> 00:21:39,589
（ちお）た… 助かりました

409
00:21:39,715 --> 00:21:41,008
（サラリーマン）危ないよ

410
00:21:41,216 --> 00:21:45,429
君たち 鮫島(さめじま)学園の生徒だろ？
名前は？

411
00:21:45,679 --> 00:21:46,722
（ちお）野々…
（真奈菜）三谷…

412
00:21:47,556 --> 00:21:48,765
（２人）うわ〜っ

413
00:21:50,976 --> 00:21:56,982
♪〜

414
00:23:10,972 --> 00:23:16,978
〜♪

415
00:23:21,358 --> 00:23:23,985
（雪）
次回「ちおちゃんの通学路」は

416
00:23:24,111 --> 00:23:26,696
「コンビニちおちゃん」
「ちおちゃんと決闘」

417
00:23:26,988 --> 00:23:29,825
「あの日のおもかげ」の
３本になります

418
00:23:29,950 --> 00:23:30,867
お楽しみに

419
00:23:31,910 --> 00:23:36,540
ハア… 久志取先輩
すごい 足 速かったな

420
00:23:36,832 --> 00:23:39,960
ちょっとぐらい もまれても
よかったかな

