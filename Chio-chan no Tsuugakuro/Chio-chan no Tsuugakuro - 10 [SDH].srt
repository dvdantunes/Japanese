﻿1
00:00:01,167 --> 00:00:07,173
♪〜

2
00:01:21,873 --> 00:01:27,879
〜♪

3
00:01:35,553 --> 00:01:38,181
（ちお）
それで お父さんが
影響されて

4
00:01:38,306 --> 00:01:40,558
庭でスモークするの
作ったんだけど

5
00:01:40,683 --> 00:01:43,937
サーモンいぶってたら
近所の猫が集まってきて

6
00:01:44,062 --> 00:01:44,979
気付いたら半分以上…

7
00:01:45,104 --> 00:01:45,939
（桃(もも)）あっ あの！

8
00:01:46,064 --> 00:01:48,149
（ちお）うわっ！　びっくりした

9
00:01:48,274 --> 00:01:49,651
桃先輩？

10
00:01:49,859 --> 00:01:52,153
急に ごめんなさい
実は…

11
00:01:53,530 --> 00:01:55,281
（２人）私たちに相談？

12
00:01:55,698 --> 00:01:57,408
はい　あの…

13
00:01:57,534 --> 00:02:00,703
お２人にしかできない
お話がありまして…

14
00:02:00,829 --> 00:02:01,746
（２人）はっ

15
00:02:02,080 --> 00:02:05,208
え〜 それって
後藤(ごとう)先生がらみですか？

16
00:02:05,333 --> 00:02:07,794
あっ はい
よく分かりましたね

17
00:02:08,169 --> 00:02:10,213
（真奈菜(まなな)）まさか… 告白？

18
00:02:11,673 --> 00:02:14,717
じ… 実は 買い食いをしたいのです

19
00:02:16,386 --> 00:02:17,220
（２人）は？

20
00:02:17,345 --> 00:02:20,682
それに後藤先生と何の関係が？

21
00:02:20,807 --> 00:02:21,975
（桃）え〜っとですね…

22
00:02:22,809 --> 00:02:23,893
先生

23
00:02:24,144 --> 00:02:27,814
中庭の片隅に これだけの
お菓子の袋が捨ててありました

24
00:02:28,189 --> 00:02:32,068
大勢で食べるなら どなたかの家に
集まって食べればいいのに

25
00:02:32,402 --> 00:02:34,404
どうして わざわざ校内で…

26
00:02:34,863 --> 00:02:36,322
（後藤先生）それは 例えば

27
00:02:36,447 --> 00:02:39,200
朝 起きなければいけないと
思うほど

28
00:02:39,325 --> 00:02:42,245
二度寝したいという気持ちが
強まるようなものだろう

29
00:02:44,289 --> 00:02:45,206
ん？

30
00:02:46,332 --> 00:02:47,792
あっ はい

31
00:02:48,877 --> 00:02:51,504
その場では つい 後藤先生のお話に

32
00:02:51,588 --> 00:02:53,673
分かったフリを
してしまいましたが…

33
00:02:53,798 --> 00:02:56,342
（ちお）
確かに 例え話が下手すぎる

34
00:02:56,509 --> 00:02:58,469
あとから自分なりに考えて

35
00:02:58,887 --> 00:03:02,140
自らも校則を破るという
経験をすることで

36
00:03:02,265 --> 00:03:06,394
その生徒たちの気持ちが理解できる
ということかと思ったのですが

37
00:03:06,895 --> 00:03:10,356
さすがに 中庭で
お菓子を食べるわけにもいかず

38
00:03:10,481 --> 00:03:13,192
その… 買い食いをと…

39
00:03:13,484 --> 00:03:14,819
しかし その…

40
00:03:15,612 --> 00:03:18,489
１人では なかなか勇気が出なくて

41
00:03:18,865 --> 00:03:20,116
分かりました

42
00:03:20,283 --> 00:03:22,577
そういうことなら
お任せください

43
00:03:25,413 --> 00:03:28,875
いや〜 でも
桃先輩は すごいですよね

44
00:03:29,250 --> 00:03:32,712
愛する人のために
自らのおきてを破るとは

45
00:03:32,837 --> 00:03:34,672
な… 何ですか その言い方は

46
00:03:35,173 --> 00:03:38,927
それに 私は
別に真面目というわけでは…

47
00:03:39,510 --> 00:03:42,263
（真奈菜）どこ行く？　猪山(いのやま)？
（ちお）そやね

48
00:03:45,725 --> 00:03:47,393
（チュ〜）

49
00:03:48,478 --> 00:03:51,564
三谷裳(みやも)さん 野々村(ののむら)さん
それは…

50
00:03:52,357 --> 00:03:53,983
器物損壊罪ですよ！

51
00:03:54,651 --> 00:03:56,694
人のうちのお花を
取ってはいけません！

52
00:03:56,819 --> 00:03:57,779
（ちお）ええ〜っ

53
00:03:58,321 --> 00:04:02,075
そんなこと言われても
昔から ここで吸ってたしな〜

54
00:04:02,325 --> 00:04:04,827
（真奈菜）おいしいね
（ちお）ね〜

55
00:04:05,036 --> 00:04:06,579
じゃあ ちょっと聞いてくる

56
00:04:08,122 --> 00:04:09,290
（真奈菜）いいって
（桃）えっ

57
00:04:09,540 --> 00:04:12,043
あの あの あの…

58
00:04:13,962 --> 00:04:16,839
桃先輩
先輩も吸ってみたいんですよね？

59
00:04:16,965 --> 00:04:17,799
ええっ？

60
00:04:18,258 --> 00:04:20,009
動かないでくださいね

61
00:04:20,134 --> 00:04:21,886
買い食いの前菜ってことで

62
00:04:22,178 --> 00:04:23,596
ダ… ダメです

63
00:04:23,721 --> 00:04:24,847
大丈夫ですよ

64
00:04:24,973 --> 00:04:27,725
私たちが 勝手に
やってるだけなんで

65
00:04:29,060 --> 00:04:30,270
（チュ〜）

66
00:04:31,312 --> 00:04:33,856
わあ 甘〜い！

67
00:04:34,107 --> 00:04:36,276
（２人）も… 桃先輩？

68
00:04:36,693 --> 00:04:40,405
え〜っ お花の蜜って
こんなに甘いんだ〜

69
00:04:40,530 --> 00:04:44,033
香りと相まって
奥深い味になってる〜

70
00:04:44,409 --> 00:04:47,954
なんだか気分はミツバ… チ

71
00:04:48,496 --> 00:04:49,330
（ちお）うっ

72
00:04:52,250 --> 00:04:54,335
（桃）や… やってしまいました

73
00:04:54,460 --> 00:04:56,587
（ちお）
あれ？　今 なんか口調が…

74
00:04:58,631 --> 00:05:02,385
（ちお）
桃先輩って 甘いものに
目がないんだ〜　意外

75
00:05:02,510 --> 00:05:03,469
はい

76
00:05:03,845 --> 00:05:05,930
目がないどころか このザマです

77
00:05:06,889 --> 00:05:08,349
家族も気味悪がって

78
00:05:08,474 --> 00:05:11,185
家でも１人のときしか
お菓子は食べられません

79
00:05:11,436 --> 00:05:12,270
えっ

80
00:05:12,395 --> 00:05:14,105
（ちお）
１人で あれ やってるのか

81
00:05:14,355 --> 00:05:16,649
お２人に同行をお願いしたのは

82
00:05:16,774 --> 00:05:19,152
買い食いしている私が
変なことをしだしたら

83
00:05:19,527 --> 00:05:22,322
止めてほしいという
お願いも兼ねてなのです

84
00:05:22,613 --> 00:05:25,241
じゃあ 甘くない
お煎餅とか食べれば…

85
00:05:28,536 --> 00:05:31,080
（真奈菜）
どうせなら
甘いお菓子が食べたいのか

86
00:05:31,622 --> 00:05:33,499
（ちお）ほら あのお店ですよ

87
00:05:33,624 --> 00:05:36,044
あそこなら
通学路からも外れてますし

88
00:05:36,169 --> 00:05:37,503
うちの生徒はいません

89
00:05:38,129 --> 00:05:40,173
ここは 店内で食べられるんですよ

90
00:05:40,298 --> 00:05:42,967
（桃）
へえ〜 こういう所は初めてです

91
00:05:43,593 --> 00:05:44,552
わあ

92
00:05:44,886 --> 00:05:47,472
（おねえさん）
だから 今日は
ばっちゃんの店番だって

93
00:05:47,597 --> 00:05:49,932
いや こういう店は 朝が早いの

94
00:05:50,224 --> 00:05:51,059
（桃）すいません

95
00:05:51,184 --> 00:05:53,144
あっ お客 来たから

96
00:05:53,269 --> 00:05:54,270
はい ど〜も

97
00:05:54,854 --> 00:05:56,522
あの… これを

98
00:05:58,941 --> 00:06:01,152
（おねえさん）
この子 なんて殺気…

99
00:06:02,612 --> 00:06:03,696
これ ください

100
00:06:03,821 --> 00:06:04,989
（おねえさん）
この子は 普通

101
00:06:14,499 --> 00:06:18,419
それでは 初めての買い食い
始めたいと思います

102
00:06:18,544 --> 00:06:19,420
（２人）はい！

103
00:06:23,633 --> 00:06:26,010
（桃）
後藤先生 お許しください

104
00:06:28,137 --> 00:06:29,097
（桃）それでは いきます！

105
00:06:29,222 --> 00:06:32,850
（おねえさん）
包み紙を あんなに
きれいに剥がす人 初めて見た

106
00:06:33,267 --> 00:06:34,102
一体…

107
00:06:34,560 --> 00:06:37,897
もし私が先ほどのように
取り乱してしまったら

108
00:06:38,564 --> 00:06:40,233
殴ってでも止めてください

109
00:06:40,358 --> 00:06:41,818
約束ですよ

110
00:06:41,943 --> 00:06:42,777
（２人）はい！

111
00:06:49,534 --> 00:06:51,619
これは… なるほど

112
00:06:51,911 --> 00:06:53,454
本格チョコとは また…

113
00:06:56,749 --> 00:06:58,251
おいしい〜！

114
00:06:58,376 --> 00:07:02,588
このカカオの風味より先に
ガツンと舌にまとわりつく甘さ

115
00:07:02,713 --> 00:07:03,548
最高です！

116
00:07:04,257 --> 00:07:05,299
（おねえさん）ええっ？

117
00:07:05,466 --> 00:07:08,052
しかも 10円なのに
中にビスケットが

118
00:07:08,177 --> 00:07:10,179
まさにサプライズ！

119
00:07:10,304 --> 00:07:13,474
この安価なハードバター特有の
口溶けも いとおかし！

120
00:07:13,599 --> 00:07:14,642
お菓子だけに！

121
00:07:15,017 --> 00:07:16,477
（２人）ああ…

122
00:07:16,727 --> 00:07:20,189
うわ〜 キャラメルが
歯に挟まる この感覚！

123
00:07:20,314 --> 00:07:22,942
うお〜 ひっさしぶり！

124
00:07:23,317 --> 00:07:24,819
（ちお）
そうか 分かった

125
00:07:25,445 --> 00:07:27,905
これが 桃先輩の素の状態なんだ

126
00:07:28,406 --> 00:07:31,033
変なことを言いだしたら
止めてくださいね

127
00:07:32,034 --> 00:07:33,119
（ちお）
思い出す

128
00:07:33,244 --> 00:07:36,205
とあるホラー系 洋ゲーの
日本版が発売されたときに

129
00:07:36,622 --> 00:07:38,458
日本メーカーの自主規制により

130
00:07:38,583 --> 00:07:42,003
暴力的なシーンが ほとんど
暗転で埋められたことを…

131
00:07:42,378 --> 00:07:46,340
臭いものに蓋をするという対処は
本当に意味があるのか？

132
00:07:46,466 --> 00:07:49,886
いや これが桃先輩の
本当の姿であるならば…

133
00:07:50,178 --> 00:07:51,053
あっ ああっ

134
00:07:51,304 --> 00:07:52,138
（ちお）
むしろ…

135
00:07:52,597 --> 00:07:53,681
（２人）
むしろ…

136
00:07:54,640 --> 00:07:57,560
（２人）
うお〜 このお菓子うめ〜！

137
00:07:58,603 --> 00:08:00,938
桃先輩も食べてみてくださいよ

138
00:08:01,063 --> 00:08:03,149
（桃）えっ いいの？
（ちお）もちろん！

139
00:08:03,274 --> 00:08:04,525
（サクサク）

140
00:08:04,775 --> 00:08:07,361
（桃）
こ… これ何？　甘ずっぱ！

141
00:08:07,487 --> 00:08:08,988
（ちお）でしょ？
（真奈菜）もう１個 どうぞ

142
00:08:09,280 --> 00:08:10,615
えっ ええ〜？

143
00:08:10,948 --> 00:08:13,201
ウエハースとクリームの食感が

144
00:08:13,326 --> 00:08:14,494
絶妙！

145
00:08:14,744 --> 00:08:17,413
（真奈菜）
このバナナのやつ うまいっすよ
うままま…

146
00:08:17,538 --> 00:08:19,123
それ 私も好き！

147
00:08:19,540 --> 00:08:22,210
ならば 私は 禁断の３種同時食い

148
00:08:24,212 --> 00:08:27,465
（桃）
北海道 お口の中が北海道！

149
00:08:27,757 --> 00:08:30,384
桃先輩
お菓子が切れそうであります

150
00:08:30,510 --> 00:08:33,387
うむ！
では 追加購入を要請いたします

151
00:08:33,971 --> 00:08:34,805
あっ

152
00:08:35,014 --> 00:08:37,892
あの… こ… これ　あっ はい

153
00:08:38,017 --> 00:08:40,186
（おねえさん）
あっ ちょっと素に戻ってる

154
00:08:40,728 --> 00:08:42,647
（ちお）先輩！　救援物資です

155
00:08:42,772 --> 00:08:43,606
でかした！

156
00:08:44,023 --> 00:08:44,899
ハハハ

157
00:08:45,107 --> 00:08:48,319
粉末飲料のコーラを
コーラに入れて

158
00:08:48,986 --> 00:08:51,239
パーフェクトコーラ 完成！

159
00:08:51,781 --> 00:08:53,658
（ゴクゴク…）

160
00:08:54,075 --> 00:08:55,284
（桃）
プハ〜ッ

161
00:08:56,118 --> 00:08:57,578
甘すぎる！

162
00:08:58,246 --> 00:08:59,372
（桃）２人も飲んで！

163
00:08:59,497 --> 00:09:00,373
ありがとうございます！

164
00:09:02,708 --> 00:09:04,585
（ちお）甘すぎる！
（桃）でしょ？

165
00:09:04,919 --> 00:09:07,588
この偉大な発明に
何か賞をください

166
00:09:07,713 --> 00:09:09,006
（真奈菜）う〜ん じゃあ

167
00:09:09,674 --> 00:09:11,467
（２人）ノーベル炭酸飲料賞！

168
00:09:11,592 --> 00:09:12,426
あっ

169
00:09:13,177 --> 00:09:14,762
そんな名誉… いいの？

170
00:09:15,263 --> 00:09:16,430
おめでとう〜

171
00:09:18,683 --> 00:09:20,268
みんな ありがとう！

172
00:09:20,726 --> 00:09:24,689
ノーベル炭酸飲料賞 受賞の
篠塚(しのづか)桃で〜す！

173
00:09:24,814 --> 00:09:26,524
（２人）
あっ これ 受賞会見だ！

174
00:09:26,774 --> 00:09:29,318
この技術を
将来 どう役立てますか？

175
00:09:29,443 --> 00:09:32,321
はい！
私はこの技術を発展させて

176
00:09:32,446 --> 00:09:33,906
いつか必ず

177
00:09:34,240 --> 00:09:37,618
後藤先生と結婚する〜！

178
00:09:41,706 --> 00:09:44,417
（桃）
どうして
止めてくれなかったんですか？

179
00:09:45,167 --> 00:09:47,670
桃先輩が
とっても幸せそうだったから

180
00:09:49,046 --> 00:09:50,923
初めての買い食いは
どうでしたか？

181
00:09:52,008 --> 00:09:54,844
（桃）それは た… た…

182
00:09:55,386 --> 00:09:57,013
楽しかったです〜

183
00:09:57,305 --> 00:09:59,348
（ちお）
えっ なんで泣いてるんですか？

184
00:09:59,473 --> 00:10:02,351
（桃）
もう二度と できないと思うと…

185
00:10:02,476 --> 00:10:03,769
（桃）ウウウ…
（２人）ええ〜っ

186
00:10:04,312 --> 00:10:07,940
（ちお）
じゃあ 今度は先輩の家で
お菓子パーティーやりましょうよ

187
00:10:08,065 --> 00:10:09,692
（桃）
えっ いいんですか？

188
00:10:13,362 --> 00:10:15,114
すみません 後藤先生

189
00:10:15,323 --> 00:10:17,617
急に お仕事を
お休みさせていただいて

190
00:10:18,326 --> 00:10:20,536
いや たまにはいいさ

191
00:10:23,539 --> 00:10:27,126
あの 私… 分かりました

192
00:10:27,543 --> 00:10:28,377
ん？

193
00:10:29,086 --> 00:10:32,048
チョコとコーラと
キャラメルの匂いがするぞ

194
00:10:32,173 --> 00:10:33,174
（桃）ううっ

195
00:10:36,510 --> 00:10:39,013
（ちお）
先輩って チョコミントは
どう思いますか？

196
00:10:39,138 --> 00:10:40,890
えっ チョコミントですか？

197
00:10:41,599 --> 00:10:44,477
かなり 好き嫌いが
分かれる味だと思いますが

198
00:10:44,852 --> 00:10:46,187
爽やかで大好きです

199
00:10:46,312 --> 00:10:47,438
（２人）えっ

200
00:10:47,938 --> 00:10:50,316
あれ 好きな人いるんだ

201
00:10:57,323 --> 00:10:58,157
ん？

202
00:11:00,159 --> 00:11:00,993
あっ

203
00:11:02,870 --> 00:11:04,872
ああっ あの子だ！

204
00:11:07,208 --> 00:11:08,709
（ちお）
真奈菜に連絡

205
00:11:13,130 --> 00:11:15,549
ん？　彼女の制服…

206
00:11:15,674 --> 00:11:18,719
永平(えいへい)女学院へ行く道とは 違う方向

207
00:11:19,053 --> 00:11:20,262
何か たくらんでる？

208
00:11:22,973 --> 00:11:26,102
（安藤(あんどう)）
ハア… ああもう まだ入ってるよ

209
00:11:26,352 --> 00:11:27,228
ったく…

210
00:11:27,353 --> 00:11:30,022
業者の車で爆発したらどうすんだ？

211
00:11:31,148 --> 00:11:32,983
ビンも隣にあるだろうが

212
00:11:33,484 --> 00:11:37,363
ハハッ この間まで 家のゴミ捨ても
したことなかったのにな

213
00:11:38,030 --> 00:11:40,074
ビンはビン 缶は缶

214
00:11:40,241 --> 00:11:41,742
うわっ 人形かよ！　怖(こわ)っ

215
00:11:45,287 --> 00:11:48,749
「地味っコ 眼鏡 220分」？

216
00:11:48,999 --> 00:11:51,085
これ 見られるかな？

217
00:11:51,210 --> 00:11:52,670
いや ダメだ

218
00:11:52,795 --> 00:11:53,879
でも ちょっとだけ…

219
00:11:54,130 --> 00:11:55,172
いや 捨てる！

220
00:11:55,339 --> 00:11:56,507
（ちはる）とりゃ！
（安藤）痛(いた)っ！

221
00:11:56,966 --> 00:11:58,092
何だ？　あっ…

222
00:11:59,218 --> 00:12:01,929
ちはる！　えっ なんでここに？

223
00:12:02,304 --> 00:12:04,723
（ちはる）
まゆ兄こそ
こんな所で何してんの？

224
00:12:05,266 --> 00:12:08,185
えっ？　コンビニのバイトだけど

225
00:12:08,310 --> 00:12:09,895
（ちはる）働くな！

226
00:12:10,229 --> 00:12:13,065
天下一の暴走族に
なるんじゃなかったの？

227
00:12:13,190 --> 00:12:14,483
いや 言ったけど…

228
00:12:14,775 --> 00:12:17,236
仕事なんてダサいこと やめてよ！

229
00:12:17,653 --> 00:12:19,780
チームやってた
まゆ兄は かっこよかったよ

230
00:12:19,905 --> 00:12:22,032
鉄と血の匂いがして
憧れてたのに

231
00:12:22,491 --> 00:12:24,285
それが 何だ この体(てい)たらくは！

232
00:12:25,244 --> 00:12:29,123
家でゴミ捨てなんか
したことないくせに〜

233
00:12:29,665 --> 00:12:33,169
これじゃ 私 何のために
私立行って勉強してるのか

234
00:12:33,586 --> 00:12:35,296
（ちお）あ… 安藤さん？

235
00:12:35,921 --> 00:12:37,965
その子 知り合いなの？

236
00:12:38,757 --> 00:12:40,301
ああ こいつは…

237
00:12:40,551 --> 00:12:42,178
（ちはる）うが〜っ
（ちお）うわ〜！

238
00:12:42,261 --> 00:12:43,304
（安藤）ちょっ 落ち着け！

239
00:12:43,637 --> 00:12:47,600
こいつは 安藤ちはる
俺の10コ下の妹だ

240
00:12:47,850 --> 00:12:50,519
えっ 何？
あんた まゆ兄の知り合い？

241
00:12:50,811 --> 00:12:52,980
（安藤）
てか 三谷裳こそ ちはると…

242
00:12:53,147 --> 00:12:55,107
この前 いきなり
カンチョーされて…

243
00:12:55,816 --> 00:12:58,819
ちょ〜っ！　何してんだ お前！

244
00:12:58,986 --> 00:13:01,405
カンチョーじゃない
正式なケツ闘だ

245
00:13:02,031 --> 00:13:06,202
華道の授業の掃除当番は
昼休みのケツ闘で決められる

246
00:13:06,327 --> 00:13:08,245
（安藤）
上品なんだか 下品なんだか

247
00:13:08,537 --> 00:13:10,789
一体 何のために三谷裳を？

248
00:13:10,915 --> 00:13:11,874
（ちはる）何のため？

249
00:13:12,333 --> 00:13:15,044
まゆ兄の“女”を見つけるため

250
00:13:15,961 --> 00:13:18,839
まゆ兄が急にチームを抜けたのが
不自然だったから

251
00:13:19,131 --> 00:13:21,133
私なりに情報を集めた

252
00:13:21,300 --> 00:13:23,511
（暴走族１）
このガキ！　何の用だ！

253
00:13:23,636 --> 00:13:25,679
（ちはる）私は安藤の妹だ！

254
00:13:26,388 --> 00:13:27,806
兄に何があった？

255
00:13:29,725 --> 00:13:30,684
（ちお）フッフッフッ
（暴走族たち）ぞっ

256
00:13:30,976 --> 00:13:31,810
教えろ！

257
00:13:32,144 --> 00:13:33,270
（暴走族１）さっ 鮫島(さめじま)の

258
00:13:33,395 --> 00:13:35,856
（暴走族２）バカ！　殺されるぞ
（暴走族１）でも…

259
00:13:36,398 --> 00:13:38,734
すまねえ 何も言えねえ

260
00:13:39,401 --> 00:13:43,614
（ちはる）
鮫島… 鮫島学園の女？

261
00:13:45,282 --> 00:13:47,451
だったら そいつを見つけて

262
00:13:49,119 --> 00:13:50,621
（ちお）
いや 殺すって…

263
00:13:51,288 --> 00:13:53,582
（ちはる）
その女を見つけるための駒として

264
00:13:53,707 --> 00:13:56,252
同じ学校の その眼鏡に
目を付けたんだ

265
00:13:56,377 --> 00:13:57,878
おい ちはる　あのな…

266
00:13:58,003 --> 00:13:59,713
隠さなくても
分かってるよ！

267
00:14:00,381 --> 00:14:03,300
（ちお）
え〜っ これって
いろいろ間違ってるけど

268
00:14:03,425 --> 00:14:05,803
私を捜してるってことだよね？

269
00:14:06,011 --> 00:14:09,181
安藤さんの件
全部 解決したと思ってたけど

270
00:14:09,306 --> 00:14:11,934
下手に名乗り出ても
いいこと なさそうだし…

271
00:14:13,769 --> 00:14:15,312
そっちで どうにかして

272
00:14:15,938 --> 00:14:18,023
えっと つまりだな…

273
00:14:18,524 --> 00:14:21,902
分かるよ
そいつ すごかったんだよね？

274
00:14:22,027 --> 00:14:22,862
（安藤）そ… そう

275
00:14:23,195 --> 00:14:26,156
（ちはる）
まゆ兄の人生観が
変わっちゃうくらいの…

276
00:14:26,365 --> 00:14:27,199
セッ×ス

277
00:14:30,661 --> 00:14:34,290
ラブホテルに住み着いてるような
肉欲の悪魔みたいな女に

278
00:14:34,415 --> 00:14:35,249
まゆ兄は…

279
00:14:35,583 --> 00:14:37,459
誰が淫魔じゃ〜！

280
00:14:37,877 --> 00:14:40,713
その反応
てめえが まゆ兄の女か！

281
00:14:40,838 --> 00:14:42,506
つきあってね〜し 処女だし

282
00:14:42,631 --> 00:14:44,675
ちはる！
どこで そんな言葉 覚えたの？

283
00:14:45,009 --> 00:14:47,386
（ちお）
てか その まゆ兄って何じゃ〜？

284
00:14:47,511 --> 00:14:49,430
（安藤）
あっ 俺 繭太(まゆた)っていうんだ

285
00:14:49,555 --> 00:14:50,639
（ちお）あっ そう！

286
00:14:51,265 --> 00:14:54,643
えっ この眼鏡が まゆ兄を？
一撃で？

287
00:14:54,935 --> 00:14:55,769
（安藤）そう！

288
00:14:55,895 --> 00:14:58,480
ちはるが言うところの決闘に
俺は負けたんだ

289
00:14:58,981 --> 00:15:02,192
そのときの約束でチームをやめた
それだけだ

290
00:15:02,318 --> 00:15:04,778
ま〜 大体そうかな

291
00:15:06,030 --> 00:15:07,281
そんなの信じられない！

292
00:15:07,823 --> 00:15:08,866
（安藤）ちはる

293
00:15:08,991 --> 00:15:11,452
俺の目がウソを言ってるように
見えるか？

294
00:15:13,829 --> 00:15:15,873
み… 見えないけど

295
00:15:15,998 --> 00:15:18,542
じゃ… じゃあ 証明して！

296
00:15:19,084 --> 00:15:22,338
まゆ兄がワンパンでってのが
どうしても信じられない

297
00:15:22,755 --> 00:15:24,757
目の前でやってくれたら 信じる

298
00:15:24,882 --> 00:15:27,468
ていうか あんな眼鏡には
絶対無理だと

299
00:15:27,593 --> 00:15:28,427
（安藤）ちはる

300
00:15:29,511 --> 00:15:32,806
（安藤）
それって 三谷裳に
俺が殴られろってことじゃ…

301
00:15:33,349 --> 00:15:34,391
（ちお）安藤さん

302
00:15:34,850 --> 00:15:35,684
（安藤）三谷裳

303
00:15:36,268 --> 00:15:37,102
ファイト

304
00:15:37,394 --> 00:15:39,021
えっ ホントにやるの？

305
00:15:39,146 --> 00:15:39,980
（ちお）当たり前でしょ！

306
00:15:40,439 --> 00:15:43,651
あんたの妹にビッチ扱いされたまま
終われないでしょ

307
00:15:43,776 --> 00:15:44,944
ホントのことだろ！

308
00:15:45,486 --> 00:15:47,029
（ちお）大丈夫 痛くしないから

309
00:15:47,154 --> 00:15:48,197
（安藤）あ… ああ

310
00:15:48,656 --> 00:15:51,367
（ちお）
あのときみたいに
あご先を打ち抜く

311
00:15:51,492 --> 00:15:52,952
いくぞ〜！

312
00:15:54,870 --> 00:15:57,414
（安藤）痛(いて)え…
（ちお）うわ〜 ごめん

313
00:15:57,998 --> 00:16:00,417
なんか緊張で
狙いが おぼつかなくて

314
00:16:00,542 --> 00:16:01,418
（安藤）お… おう

315
00:16:02,962 --> 00:16:04,254
（ちお）もう１回
（安藤）おう！

316
00:16:04,797 --> 00:16:08,634
（安藤）
集中しろ　俺が動きを合わせて
あごに当てさせるんだ

317
00:16:08,842 --> 00:16:10,886
（ちお）いくね
（安藤）よし いつでも

318
00:16:11,553 --> 00:16:12,513
（安藤）
まさかの逆！

319
00:16:12,805 --> 00:16:14,264
ひ… 左？

320
00:16:14,390 --> 00:16:16,934
いや ごめん
右ひじ痛かったから

321
00:16:17,184 --> 00:16:18,352
ほら ウソじゃん！

322
00:16:18,769 --> 00:16:20,604
（ちお）ラスト
（安藤）よし 来い！

323
00:16:21,522 --> 00:16:23,983
（安藤）
あのときを 再現するんだ

324
00:16:24,733 --> 00:16:26,694
そのためには 三谷裳を

325
00:16:27,069 --> 00:16:28,529
驚かせる！

326
00:16:30,280 --> 00:16:31,115
（ちお）いっ

327
00:16:31,740 --> 00:16:33,242
（安藤）
さあ 来い

328
00:16:33,742 --> 00:16:35,035
あ… あれ？

329
00:16:35,160 --> 00:16:36,412
（ちはる）何？　どうした？

330
00:16:37,913 --> 00:16:40,833
はっ えっ あ… あれ？
えっ あっ いや…

331
00:16:40,958 --> 00:16:41,917
（安藤）その… あの…

332
00:16:42,501 --> 00:16:45,337
え〜っと どう… だった？

333
00:16:45,546 --> 00:16:48,090
ど… どうって あの… えっ？

334
00:16:48,507 --> 00:16:50,342
（安藤）あらららら…
（ちお）あああ… あれ？

335
00:16:50,467 --> 00:16:53,178
（ちお）
いや あの… 私 何 言ってるんだ？
だから…

336
00:16:53,303 --> 00:16:55,097
（安藤）俺 あ… 悪意とか

337
00:16:55,222 --> 00:16:56,515
（ちはる）ちょっと 何なの？

338
00:16:56,682 --> 00:16:59,685
（安藤）悪かっ…
（真奈菜）邪魔じゃ〜！

339
00:17:00,978 --> 00:17:05,899
ついに見つけたぞ　このクソガキ

340
00:17:06,400 --> 00:17:07,359
はっ

341
00:17:07,526 --> 00:17:09,236
（ちお）
そうだった　真奈菜…

342
00:17:09,361 --> 00:17:11,613
ちはるちゃんに
ケツをえらい目に遭わされたから

343
00:17:11,739 --> 00:17:13,282
めちゃめちゃ キレてるんだった

344
00:17:13,449 --> 00:17:15,034
（ちはる）ま… まゆ兄 大丈夫？

345
00:17:15,159 --> 00:17:17,369
（ちお）
安藤さん ただ近くに
立ってただけなのに

346
00:17:17,745 --> 00:17:19,997
ま… まゆ兄が一撃で

347
00:17:20,122 --> 00:17:21,457
ま… まさか

348
00:17:22,249 --> 00:17:24,585
（ちはる）
あの女こそが まゆ兄の…

349
00:17:25,377 --> 00:17:27,671
まゆ兄の 本当の女

350
00:17:27,796 --> 00:17:31,091
ケツカラ ワタヌイテ
ツケモノ ツケル…

351
00:17:32,801 --> 00:17:35,929
上等だ こら！
かかってこいや〜！

352
00:17:36,055 --> 00:17:36,972
（ちお）ダ… ダメだよ

353
00:17:37,556 --> 00:17:39,975
あの女はキレるとヤバいんだって！

354
00:17:40,142 --> 00:17:41,435
将来 彼氏が浮気したら

355
00:17:41,560 --> 00:17:44,438
バラバラにして
海にまくようなヤツなんだって！

356
00:17:44,938 --> 00:17:47,524
（ちお）
ヤバい どうしよう
私じゃ止められない

357
00:17:48,150 --> 00:17:49,068
死人が出る

358
00:17:49,943 --> 00:17:50,778
（真奈菜･ちはる）ふっ！

359
00:17:52,029 --> 00:17:53,030
あっ

360
00:17:54,198 --> 00:17:55,699
ま… まゆ兄！

361
00:17:55,908 --> 00:17:58,160
なっ なんで？
気を失ってたんじゃ…

362
00:18:02,706 --> 00:18:06,752
やめろ　妹に手を出したら殺す

363
00:18:07,336 --> 00:18:09,671
（ちお）
ダメだ　安藤さん
さっき やられてたし

364
00:18:10,089 --> 00:18:12,216
真奈菜は 私が止める！

365
00:18:12,549 --> 00:18:14,134
あっ はい　分かりました

366
00:18:14,259 --> 00:18:15,135
（ちお）ズコ〜ッ！

367
00:18:16,220 --> 00:18:18,055
（ちお）
えっ 何？　この聞き分けのよさ

368
00:18:18,639 --> 00:18:20,015
はっ そうか

369
00:18:20,140 --> 00:18:22,935
忘れがちだけど
安藤さんって不良だったから

370
00:18:23,310 --> 00:18:25,854
もともと スクールカースト
上位じゃん！

371
00:18:25,979 --> 00:18:30,234
そりゃ 本能的に 私たち中の下が
戦えるわけがないんだった

372
00:18:32,152 --> 00:18:33,445
逆に言えば

373
00:18:34,113 --> 00:18:37,074
小学生みたいな
自分より下の存在になら

374
00:18:37,199 --> 00:18:38,909
どこまでも残酷になれる

375
00:18:39,910 --> 00:18:42,996
怖いわ〜 こいつ怖いわ〜

376
00:18:43,539 --> 00:18:45,874
（ちはる）
まゆ兄 気を失ったまま

377
00:18:46,208 --> 00:18:48,752
無意識に私のこと守って…

378
00:18:51,255 --> 00:18:52,256
（客）あっ いた

379
00:18:52,714 --> 00:18:55,217
ちょっと 店員さん
レジ待ってるんだけど！

380
00:18:55,634 --> 00:18:58,011
はっ すいません！
今 行きます！

381
00:18:58,137 --> 00:19:00,848
（客）
何やってるの　しっかりしてよ

382
00:19:03,851 --> 00:19:06,895
一件落着… でいいのかな？

383
00:19:07,479 --> 00:19:10,065
あ… あの
結局 私たちは ただの…

384
00:19:10,190 --> 00:19:11,108
（ちはる）分かったよ

385
00:19:11,942 --> 00:19:14,486
マジの彼女だったら
あんなにキレないだろうし

386
00:19:14,611 --> 00:19:15,988
まあ それに…

387
00:19:16,655 --> 00:19:19,366
（安藤）
どうもすいません ヘヘッ
ありがとうございました！

388
00:19:21,451 --> 00:19:23,453
働く姿も悪くない？

389
00:19:24,037 --> 00:19:25,581
うるせえ！　バカ！

390
00:19:25,747 --> 00:19:26,582
ひいっ

391
00:19:27,082 --> 00:19:29,293
（ちはる）
分かったようなこと言うな！
ったく

392
00:19:30,252 --> 00:19:31,795
あっ 気は済んだ？

393
00:19:32,045 --> 00:19:34,965
ヤンキー 怖い〜！

394
00:19:35,090 --> 00:19:36,175
（ちお）あっ ダメだ

395
00:19:38,510 --> 00:19:41,388
ああ〜 結局 何もできなかった

396
00:19:41,555 --> 00:19:45,267
（ちお）
いや 真奈菜 安藤さんのこと
ぶん殴ってたけどね

397
00:19:45,392 --> 00:19:46,310
えっ？

398
00:19:46,518 --> 00:19:48,562
（ちお）
覚えてないかもしれないけど
２人とも

399
00:19:48,854 --> 00:19:52,065
そういえば じんわりと感触がある

400
00:19:52,357 --> 00:19:54,860
そう思うと ちょっとスッキリ

401
00:19:55,235 --> 00:19:57,946
（ちお）
八つ当たりできれば
誰でもよかったのか…

402
00:19:59,114 --> 00:20:00,657
（ニャ〜オ）

403
00:20:06,955 --> 00:20:08,749
（ニャ〜オ）

404
00:20:16,173 --> 00:20:18,091
（ニャ〜オ）

405
00:20:42,908 --> 00:20:43,784
（ピピッ）

406
00:21:06,598 --> 00:21:07,432
（パキッ）

407
00:21:16,358 --> 00:21:17,276
（ガンッ）

408
00:21:28,412 --> 00:21:29,579
（キキ〜ッ）

409
00:21:29,705 --> 00:21:30,706
（ガシャン！）

410
00:21:56,606 --> 00:21:57,441
（ニャッ）

411
00:21:59,026 --> 00:22:00,193
（ニャ〜オ）

412
00:22:01,028 --> 00:22:07,034
♪〜

413
00:23:20,982 --> 00:23:26,988
〜♪

414
00:23:31,076 --> 00:23:32,744
（ちお）
次回「ちおちゃんの通学路」は

415
00:23:33,078 --> 00:23:36,081
「真夜中のちおちゃん」
「あぽくり！」の２本になります

416
00:23:36,206 --> 00:23:37,082
お楽しみに

417
00:23:37,207 --> 00:23:38,500
誰が淫魔じゃ〜！

418
00:23:38,834 --> 00:23:40,418
でも どう… だった？

