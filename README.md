
Okashi - All the subs you were wishing for but never asked for. 

- [DESCRIPTION](#description)
- [FAQ](#faq) 
- [COPYRIGHT](#copyright)

# DESCRIPTION

**Okashi** is a community project that aims to recover all the Japanese anime subtitles.

# FAQ

#### I have a subtitle synchronization problem

 - Synchronization problems are not our responsibility.
 
#### Where find the videos ?

- We will not provide the corresponding videos.

#### How to contribute by sharing your subtitles ?

- Create a ticket with the information.

# COPYRIGHT

Okashi is released into the public domain by the copyright holders.
